# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]


## Release v0.15.14 - 2024-11-28(10:19:29 +0000)

### Other

- - dump config of lighttpd into debug.txt

## Release v0.15.13 - 2024-11-07(15:34:55 +0000)

### Other

- - make lighttpd restart on failling

## Release v0.15.12 - 2024-11-04(09:26:59 +0000)

### Fixes

- [tr181-httpaccess] source environment in init script

## Release v0.15.11 - 2024-10-04(11:54:58 +0000)

### Other

- - Intermittent Web GUI page inaccessibility after WAN reset.

## Release v0.15.10 - 2024-09-10(07:14:35 +0000)

### Other

- [AppArmor] Create AppAmor profile for plugins

## Release v0.15.9 - 2024-09-05(11:21:13 +0000)

### Other

- [HttpAccess] An incorrect bus context is used for amxb calls

## Release v0.15.8 - 2024-08-13(13:54:47 +0000)

### Other

- - [UserInterface] missing Available­Languages and CurrentLanguage DM

## Release v0.15.7 - 2024-07-30(17:07:31 +0000)

### Other

- random: tr181-httpaccess: REST API and web user interface is not available

## Release v0.15.6 - 2024-07-24(06:43:14 +0000)

### Other

- [System Upgrade][Remote administration] Changes in Remote Administration didn't Upgrade successfully

## Release v0.15.5 - 2024-07-23(07:53:49 +0000)

### Fixes

- Better shutdown script

## Release v0.15.4 - 2024-07-11(05:56:14 +0000)

### Fixes

- [LOG]The Home CPE SHALL have the ability to log all attempts...

## Release v0.15.3 - 2024-07-08(06:57:51 +0000)

## Release v0.15.2 - 2024-05-28(13:24:09 +0000)

### Fixes

- - Add support for LLA addresses and expose both LLA and GUA addresses simultaneously

## Release v0.15.1 - 2024-04-10(07:08:52 +0000)

### Changes

- Make amxb timeouts configurable

## Release v0.15.0 - 2024-03-25(07:36:13 +0000)

### New

- Add tr181-device proxy odl files to components

## Release v0.14.3 - 2024-03-19(16:17:16 +0000)

### Other

- Rework TR-181 interface stacks indexing number

## Release v0.14.2 - 2024-03-19(12:59:18 +0000)

### Other

- Rework TR-181 interface stacks indexing number

## Release v0.14.1 - 2024-03-06(15:42:06 +0000)

### Fixes

- - Ignore netmodel ipv6 addresses during duplicate address detection (DAD)

## Release v0.14.0 - 2024-01-30(08:43:40 +0000)

### New

- - Manage source IP filtering using parameters 'Allow­All­IPv­[4/6]' and 'IPv[4/6]AllowedSourcePrefix' (firewall)

## Release v0.13.1 - 2024-01-25(12:05:11 +0000)

### Other

- [CI][HTTPAccess] Plugin not starting due to race condition with Device.Users

## Release v0.13.0 - 2024-01-18(13:34:13 +0000)

### New

- - Add IP V6 support through protected parameter UserInterface.HTTPAccess.{i}.IPVersion

## Release v0.12.0 - 2024-01-11(15:52:03 +0000)

### Other

- [CI][HTTPAccess] Plugin not starting due to race condition with Device.Users

## Release v0.11.4 - 2024-01-11(09:53:36 +0000)

### Other

- [CI][HTTPAccess] Plugin not starting due to race condition with Device.Users

## Release v0.11.3 - 2023-12-15(11:11:35 +0000)

## Release v0.11.2 - 2023-12-14(16:41:33 +0000)

### Other

- [prpl][tr181-httpaccess] Session isn't deleted after closed and may increase memory consumption

## Release v0.11.1 - 2023-12-09(10:33:31 +0000)

### Other

- [tr181-httpaccess]The defined data model is not behaving as defined in TR-181 usp

## Release v0.11.0 - 2023-12-07(11:02:05 +0000)

### New

- - Allow inclusion of additional lighttpd config under dynamically generated sections

## Release v0.10.3 - 2023-11-20(10:11:11 +0000)

### Fixes

- - [prpl CI][lighttpd] restarts during cram tests

## Release v0.10.2 - 2023-11-13(10:25:18 +0000)

### Other

- - [HTTPAccess][mop] A status change in the 'HTTPAccess' instances doesnt generate an event

## Release v0.10.1 - 2023-11-07(12:46:28 +0000)

### Changes

- - [prpl][tr181-httpaccess] Use amxc_set_to_string_sep instead of manipulating string manually

## Release v0.10.0 - 2023-11-06(09:50:05 +0000)

### New

- - [UI][LAN versus WAN admin] Allow the WAN and the LAN UI to have different ACL(users)

## Release v0.9.0 - 2023-10-19(08:55:37 +0000)

### New

- Add functionality for a UI onboarding scenario

## Release v0.8.1 - 2023-10-13(13:39:50 +0000)

### Changes

- All applications using sahtrace logs should use default log levels

## Release v0.8.0 - 2023-10-12(11:32:46 +0000)

### New

- [amxrt][no-root-user][capability drop] All amx plugins must be adapted to run as non-root and lmited capabilities

## Release v0.7.1 - 2023-09-28(08:49:00 +0000)

### Other

- [AP config][HTTPD] The full featured config must support a webserver

## Release v0.7.0 - 2023-09-18(09:27:53 +0000)

### New

- [tr181-httpaccess] API support for number of unsuccessful login attempts

## Release v0.6.7 - 2023-09-08(08:04:19 +0000)

### Fixes

- - Device.­User­Interface.­HTTPAccess.­{i}. Issues

## Release v0.6.6 - 2023-08-29(06:52:55 +0000)

### Fixes

- Plugin should wait for Users.Role

## Release v0.6.5 - 2023-08-24(10:15:17 +0000)

### Fixes

- Init script has no shutdown function

## Release v0.6.4 - 2023-08-22(06:55:49 +0000)

### Fixes

- - [prpl][user-management] Users role paths are not tr181 paths

## Release v0.6.3 - 2023-07-19(09:44:12 +0000)

### Other

- - [regression] Cannot login anymore from WebUI on 0.7.2

## Release v0.6.2 - 2023-07-17(07:29:15 +0000)

### Changes

- - HTTP Access AllowedRoles defined with an incorrect data type

## Release v0.6.1 - 2023-07-17(07:21:17 +0000)

### Other

- - [HTTPManager][Login] Increase unit test coverage

## Release v0.6.0 - 2023-06-28(14:38:09 +0000)

### New

- [amx][prpl]Implementation of the LANConfigSecurity module

## Release v0.5.0 - 2023-06-19(08:57:59 +0000)

### New

- [amx][prpl]Implementation of the LANConfigSecurity module

## Release v0.4.0 - 2023-06-06(10:35:44 +0000)

### New

- [amx][prpl]Implementation of the LANConfigSecurity module

## Release v0.3.3 - 2023-05-23(11:10:41 +0000)

### Fixes

- - [FIX] use deferred call to deinit session after a timeout

## Release v0.3.2 - 2023-05-12(10:25:36 +0000)

### Other

- - [HTTP-Manager] Improve documentation

## Release v0.3.1 - 2023-05-11(09:27:37 +0000)

### Fixes

- [tr181-httpaccess] debian package is incorrect

## Release v0.3.0 - 2023-05-09(13:45:47 +0000)

### New

- - [HTTPManager][Login][TR181-HTTPACCESS] Create a session

## Release v0.2.10 - 2023-04-21(10:16:53 +0000)

### Fixes

- service protocol should be integers

## Release v0.2.9 - 2023-03-23(12:51:16 +0000)

### Other

- - [tr181-httpaccess] Add Firewall datamodel runtime dependency

## Release v0.2.8 - 2023-03-15(10:55:19 +0000)

### Changes

- [Config] enable configurable coredump generation

## Release v0.2.7 - 2023-01-09(10:35:16 +0000)

### Fixes

- avoidable copies of strings, htables and lists

## Release v0.2.6 - 2023-01-09(09:51:13 +0000)

### Fixes

- init without clean on local variables

## Release v0.2.5 - 2023-01-04(09:47:45 +0000)

### Fixes

- Port 8080 opened on WAN when using VLAN as wan

## Release v0.2.4 - 2022-12-13(09:00:57 +0000)

### Changes

- Refactor HTTPVHost and HTTPProxy to be HTTPAccess

## Release v0.2.3 - 2022-11-21(11:50:27 +0000)

### Other

- Opensource component

## Release v0.2.2 - 2022-11-18(16:39:06 +0000)

### Fixes

- Config file stored at wrong location

## Release v0.2.1 - 2022-11-16(08:41:57 +0000)

### Other

- Change default configuration file paths

## Release v0.2.0 - 2022-11-16(07:59:51 +0000)

### New

- - [prpl][httpaccess] Create the HTTPAccess backend

## Release v0.1.0 - 2022-09-22(08:37:54 +0000)

### New

- - [prpl][httpaccess] Create the HTTPAccess datamodel

### Fixes

- Remove pedantic flag

