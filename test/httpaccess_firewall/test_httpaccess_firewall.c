/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2024 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdlib.h>
#include <setjmp.h>
#include <stdarg.h>
#include <cmocka.h>
#include <string.h>

#include <amxc/amxc.h>
#include <amxp/amxp.h>
#include <amxd/amxd_dm.h>
#include <amxd/amxd_object.h>
#include <amxd/amxd_transaction.h>
#include <amxo/amxo.h>
#include <amxb/amxb.h>

#include "dm_tr181-httpaccess.h"
#include "httpaccess_firewall.h"
#include "tr181-httpaccess.h"
#include "httpaccess_access.h"

#include "test_httpaccess_firewall.h"

static amxd_dm_t dm;
static amxo_parser_t parser;
static const char* odl_defs = "httpaccess_test.odl";

static void handle_events(void) {
    while(amxp_signal_read() == 0) {
    }
}

static amxd_object_t* get_httpaccess_object() {
    amxd_object_t* httpaccess_object = NULL;

    httpaccess_object = amxd_dm_findf(httpaccess_get_dm(), HTTPACCESS_TR181_DEVICE_USERINTERFACE_HTTPACCESS_PATH);
    assert_non_null(httpaccess_object);

    return httpaccess_object;
}

static amxd_object_t* create_access_instance(const char* access_name,
                                             bool enable,
                                             const char* type,
                                             uint32_t port,
                                             const char* interface,
                                             uint8_t ipversion,
                                             bool allowallipv4,
                                             const char* ipv4allowedsourceprefix,
                                             bool allowallipv6,
                                             const char* ipv6allowedsourceprefix) {
    amxd_object_t* access;
    amxc_var_t params;
    amxc_var_init(&params);
    amxc_var_set_type(&params, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &params, "Alias", access_name);
    amxd_object_new_instance(&access, get_httpaccess_object(), access_name, 0, &params);
    amxd_object_set_cstring_t(access, "Alias", access_name);

    amxd_object_set_bool(access, "Enable", enable);
    amxd_object_set_cstring_t(access, "AccessType", type);
    amxd_object_set_uint32_t(access, "Port", port);
    amxd_object_set_cstring_t(access, "Interface", interface);
    amxd_object_set_uint8_t(access, "IPVersion", ipversion);
    amxd_object_set_bool(access, "AllowAllIPv4", allowallipv4);
    amxd_object_set_csv_string_t(access, "IPv4AllowedSourcePrefix", ipv4allowedsourceprefix);
    amxd_object_set_bool(access, "AllowAllIPv6", allowallipv6);
    amxd_object_set_csv_string_t(access, "IPv6AllowedSourcePrefix", ipv6allowedsourceprefix);

    amxc_var_clean(&params);
    return access;
}

int __wrap_httpaccess_mod_init(void) {
    return 0;
}

int __wrap_amxm_so_open(UNUSED amxm_shared_object_t** so, UNUSED const char* shared_object_name, UNUSED const char* const path_to_so) {
    return 0;
}

int __wrap_amxm_execute_function(UNUSED const char* const shared_object_name,
                                 UNUSED const char* const module_name,
                                 UNUSED const char* const func_name,
                                 UNUSED amxc_var_t* args,
                                 amxc_var_t* ret) {

    int retval = mock_type(int);
    const char* firewall_rule_id = GET_CHAR(args, "id");
    const char* interface = GET_CHAR(args, "interface");
    uint32_t destination_port = GET_UINT32(args, "destination_port");
    uint32_t ip_version = GET_UINT32(args, "ipversion");
    const char* source_prefix = GET_CHAR(args, "source_prefix");
    const char* protocol = GET_CHAR(args, "protocol");
    bool enable = GET_BOOL(args, "enable");

    assert_string_equal("fw", shared_object_name);
    assert_string_equal("fw", module_name);
    assert_string_equal("set_service", func_name);

    assert_string_equal("6", protocol);
    assert_true(enable);

    check_expected(firewall_rule_id);
    check_expected(interface);
    check_expected(destination_port);
    check_expected(ip_version);
    check_expected(source_prefix);

    amxc_var_set_type(ret, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, ret, "id", GET_CHAR(args, "id"));
    return retval;
}

static void check_expected_firewall_service(const char* fw_id, const char* interface_name, uint32_t port, UNUSED uint32_t ipversion, const char* allowed_source_prefix) {
    expect_string(__wrap_amxm_execute_function, firewall_rule_id, fw_id);
    expect_string(__wrap_amxm_execute_function, interface, interface_name);
    expect_value(__wrap_amxm_execute_function, destination_port, port);
    expect_value(__wrap_amxm_execute_function, ip_version, ipversion);
    expect_string(__wrap_amxm_execute_function, source_prefix, allowed_source_prefix);
}


bool __wrap_amxm_has_function(UNUSED const char* const shared_object_name,
                              UNUSED const char* const module_name,
                              UNUSED const char* const func_name) {
    return true;
}

int test_httpaccess_firewall_teardown(UNUSED void** state) {
    amxc_var_t* rules = httpaccess_firewall_get_current_rules();
    amxc_var_clean(rules);
    amxc_var_set_type(rules, AMXC_VAR_ID_HTABLE);
    return 0;
}

static bool firewall_rule_contains_access(const amxc_var_t* fw_rule, const char* access_name) {

    bool is_present = false;
    amxc_var_for_each(access, fw_rule) {
        const cstring_t str = amxc_var_get_const_cstring_t(access);
        if(strcmp(str, access_name) == 0) {
            is_present = true;
            break;
        }
    }

    return is_present;
}

int test_httpaccess_firewall_group_setup(UNUSED void** state) {
    amxd_object_t* root_obj = NULL;

    assert_int_equal(amxd_dm_init(&dm), amxd_status_ok);
    assert_int_equal(amxo_parser_init(&parser), 0);

    root_obj = amxd_dm_get_root(&dm);
    assert_non_null(root_obj);

    assert_int_equal(amxo_resolver_ftab_add(&parser, "dm_httpaccess_changed",
                                            AMXO_FUNC(_dm_httpaccess_changed)), 0);
    assert_int_equal(amxo_resolver_ftab_add(&parser, "dm_httpaccess_added",
                                            AMXO_FUNC(_dm_httpaccess_added)), 0);
    assert_int_equal(amxo_resolver_ftab_add(&parser, "dm_httpaccess_destroyed",
                                            AMXO_FUNC(_dm_httpaccess_destroyed)), 0);
    assert_int_equal(amxo_resolver_ftab_add(&parser, "dm_httpaccess_ip_filtering_changed",
                                            AMXO_FUNC(_dm_httpaccess_ip_filtering_changed)), 0);
    assert_int_equal(amxo_parser_parse_file(&parser, odl_defs, root_obj), 0);

    _httpaccess_main(0, &dm, &parser);

    handle_events();
    return 0;
}

int test_httpaccess_firewall_group_teardown(UNUSED void** state) {
    _httpaccess_main(1, &dm, &parser);
    amxo_parser_clean(&parser);
    amxd_dm_clean(&dm);
    return 0;
}

void test_httpaccess_firewall_add_access_rules_with_remote_access(UNUSED void** state) {
    amxd_object_t* my_access1 = NULL;
    amxd_object_t* my_access2 = NULL;

    my_access1 = create_access_instance("my-access1",
                                        true,
                                        "RemoteAccess",
                                        81,
                                        "Device.IP.Interface.1",
                                        0,
                                        true,
                                        "",
                                        true,
                                        "");

    my_access2 = create_access_instance("my-access2",
                                        true,
                                        "RemoteAccess",
                                        80,
                                        "Device.IP.Interface.2",
                                        0,
                                        false,
                                        "plop1, plop2, plop3",
                                        false,
                                        "plop4, plop5, plop6");

    will_return_count(__wrap_amxm_execute_function, 0, 4);

    // for my-access1
    check_expected_firewall_service("tr181-httpaccess-Device-IP-Interface-1-v4-81",
                                    "Device.IP.Interface.1", 81, 4, "");

    check_expected_firewall_service("tr181-httpaccess-Device-IP-Interface-1-v6-81",
                                    "Device.IP.Interface.1", 81, 6, "");

    // for my-access2
    check_expected_firewall_service("tr181-httpaccess-Device-IP-Interface-2-v4-80",
                                    "Device.IP.Interface.2", 80, 4, "plop1, plop2, plop3");

    check_expected_firewall_service("tr181-httpaccess-Device-IP-Interface-2-v6-80",
                                    "Device.IP.Interface.2", 80, 6, "plop4, plop5, plop6");

    assert_int_equal(httpaccess_firewall_add_access("my-access1"), 0);
    assert_int_equal(httpaccess_firewall_add_access("my-access2"), 0);

    amxd_object_delete(&my_access1);
    amxd_object_delete(&my_access2);
}

void test_httpaccess_firewall_add_access_rules_with_local_access(UNUSED void** state) {
    amxd_object_t* my_access1 = NULL;
    amxd_object_t* my_access2 = NULL;

    my_access1 = create_access_instance("my-access1",
                                        true,
                                        "LocalAccess",
                                        81,
                                        "Device.IP.Interface.1",
                                        0,
                                        true,
                                        "plop1, plop2, plop3",
                                        true,
                                        "plop4, plop5, plop6");

    my_access2 = create_access_instance("my-access2",
                                        true,
                                        "LocalAccess",
                                        80,
                                        "Device.IP.Interface.2",
                                        0,
                                        false,
                                        "plop1, plop2, plop3",
                                        false,
                                        "plop4, plop5, plop6");

    will_return_count(__wrap_amxm_execute_function, 0, 4);

    // for my-access1
    check_expected_firewall_service("tr181-httpaccess-Device-IP-Interface-1-v4-81",
                                    "Device.IP.Interface.1", 81, 4, "");

    check_expected_firewall_service("tr181-httpaccess-Device-IP-Interface-1-v6-81",
                                    "Device.IP.Interface.1", 81, 6, "");

    // for my-access2
    check_expected_firewall_service("tr181-httpaccess-Device-IP-Interface-2-v4-80",
                                    "Device.IP.Interface.2", 80, 4, "");

    check_expected_firewall_service("tr181-httpaccess-Device-IP-Interface-2-v6-80",
                                    "Device.IP.Interface.2", 80, 6, "");

    assert_int_equal(httpaccess_firewall_add_access("my-access1"), 0);
    assert_int_equal(httpaccess_firewall_add_access("my-access2"), 0);

    amxd_object_delete(&my_access1);
    amxd_object_delete(&my_access2);
}

void test_httpaccess_firewall_add_access_rules_mutualized(UNUSED void** state) {
    amxd_object_t* my_access1 = NULL;
    amxd_object_t* my_access2 = NULL;
    amxc_var_t* rules = NULL;
    amxc_var_t* ipv4_rule = NULL;
    amxc_var_t* ipv6_rule = NULL;

    my_access1 = create_access_instance("my-access1",
                                        true,
                                        "RemoteAccess",
                                        8090,
                                        "Device.IP.Interface.1",
                                        0,
                                        true,
                                        "",
                                        true,
                                        "");

    my_access2 = create_access_instance("my-access2",
                                        true,
                                        "RemoteAccess",
                                        8090,
                                        "Device.IP.Interface.1",
                                        0,
                                        true,
                                        "",
                                        true,
                                        "");

    will_return_count(__wrap_amxm_execute_function, 0, 2); // only 2 calls since rules already exist

    check_expected_firewall_service("tr181-httpaccess-Device-IP-Interface-1-v4-8090",
                                    "Device.IP.Interface.1", 8090, 4, "");

    check_expected_firewall_service("tr181-httpaccess-Device-IP-Interface-1-v6-8090",
                                    "Device.IP.Interface.1", 8090, 6, "");

    assert_int_equal(httpaccess_firewall_add_access("my-access1"), 0);
    assert_int_equal(httpaccess_firewall_add_access("my-access2"), 0);

    rules = httpaccess_firewall_get_current_rules();
    assert_non_null(rules);
    ipv4_rule = amxc_var_get_key(rules, "tr181-httpaccess-Device-IP-Interface-1-v4-8090", AMXC_VAR_FLAG_DEFAULT);
    assert_non_null(ipv4_rule);
    assert_true(firewall_rule_contains_access(ipv4_rule, "my-access1"));
    assert_true(firewall_rule_contains_access(ipv4_rule, "my-access2"));

    ipv6_rule = amxc_var_get_key(rules, "tr181-httpaccess-Device-IP-Interface-1-v6-8090", AMXC_VAR_FLAG_DEFAULT);
    assert_non_null(ipv6_rule);
    assert_true(firewall_rule_contains_access(ipv6_rule, "my-access1"));
    assert_true(firewall_rule_contains_access(ipv6_rule, "my-access2"));

    amxd_object_delete(&my_access1);
    amxd_object_delete(&my_access2);
}

void test_httpaccess_firewall_add_non_existing_access(UNUSED void** state) {
    assert_int_equal(httpaccess_firewall_add_access("non-existing-access"), -1);
}

void test_httpaccess_firewall_add_access_rules_with_access_disable(UNUSED void** state) {
    amxd_object_t* my_access = NULL;
    amxd_object_t* my_access_disable = NULL;

    my_access = create_access_instance("my-access",
                                       true,
                                       "RemoteAccess",
                                       80,
                                       "Device.IP.Interface.1",
                                       0,
                                       true,
                                       "",
                                       true,
                                       "");

    my_access_disable = create_access_instance("my-access-disable",
                                               false,
                                               "RemoteAccess",
                                               80,
                                               "Device.IP.Interface.3",
                                               0,
                                               true,
                                               "",
                                               true,
                                               "");

    will_return_count(__wrap_amxm_execute_function, 0, 2);

    check_expected_firewall_service("tr181-httpaccess-Device-IP-Interface-1-v4-80",
                                    "Device.IP.Interface.1", 80, 4, "");

    check_expected_firewall_service("tr181-httpaccess-Device-IP-Interface-1-v6-80",
                                    "Device.IP.Interface.1", 80, 6, "");

    assert_int_equal(httpaccess_firewall_add_access("my-access"), 0);
    assert_int_equal(httpaccess_firewall_add_access("my-access-disable"), 0);

    amxd_object_delete(&my_access);
    amxd_object_delete(&my_access_disable);
}

void test_httpaccess_firewall_add_access_rules_with_ipv4_access_only(UNUSED void** state) {
    amxd_object_t* my_access_ipv4_only = NULL;

    my_access_ipv4_only = create_access_instance("my-access-ipv4-only",
                                                 true,
                                                 "RemoteAccess",
                                                 80,
                                                 "Device.IP.Interface.1",
                                                 4,
                                                 true,
                                                 "",
                                                 true,
                                                 "");

    will_return_count(__wrap_amxm_execute_function, 0, 1);

    check_expected_firewall_service("tr181-httpaccess-Device-IP-Interface-1-v4-80",
                                    "Device.IP.Interface.1", 80, 4, "");

    assert_int_equal(httpaccess_firewall_add_access("my-access-ipv4-only"), 0);

    amxd_object_delete(&my_access_ipv4_only);
}

void test_httpaccess_firewall_add_access_rules_with_ipv6_access_only(UNUSED void** state) {
    amxd_object_t* my_access_ipv6_only = NULL;

    my_access_ipv6_only = create_access_instance("my-access-ipv6-only",
                                                 true,
                                                 "RemoteAccess",
                                                 80,
                                                 "Device.IP.Interface.1",
                                                 6,
                                                 true,
                                                 "",
                                                 true,
                                                 "");

    will_return_count(__wrap_amxm_execute_function, 0, 1);

    check_expected_firewall_service("tr181-httpaccess-Device-IP-Interface-1-v6-80",
                                    "Device.IP.Interface.1", 80, 6, "");

    assert_int_equal(httpaccess_firewall_add_access("my-access-ipv6-only"), 0);

    amxd_object_delete(&my_access_ipv6_only);
}


void test_httpaccess_firewall_add_access_rules_with_access_allowing_all_ipv4_disable(UNUSED void** state) {
    amxd_object_t* my_access = NULL;

    my_access = create_access_instance("my-access",
                                       true,
                                       "RemoteAccess",
                                       8080,
                                       "Device.IP.Interface.1",
                                       4,
                                       false,
                                       "plop1, plop2, plop3, plop4",
                                       true,
                                       "");

    will_return_count(__wrap_amxm_execute_function, 0, 1);
    check_expected_firewall_service("tr181-httpaccess-Device-IP-Interface-1-v4-8080",
                                    "Device.IP.Interface.1", 8080, 4, "plop1, plop2, plop3, plop4");

    assert_int_equal(httpaccess_firewall_add_access("my-access"), 0);

    amxd_object_delete(&my_access);
}

void test_httpaccess_firewall_add_access_rules_with_access_allowing_all_ipv6_disable(UNUSED void** state) {
    amxd_object_t* my_access = NULL;

    my_access = create_access_instance("my-access",
                                       true,
                                       "RemoteAccess",
                                       8080,
                                       "Device.IP.Interface.1",
                                       6,
                                       true,
                                       "",
                                       false,
                                       "plop2, plop3, plop4");


    will_return_count(__wrap_amxm_execute_function, 0, 1);
    check_expected_firewall_service("tr181-httpaccess-Device-IP-Interface-1-v6-8080",
                                    "Device.IP.Interface.1", 8080, 6, "plop2, plop3, plop4");

    assert_int_equal(httpaccess_firewall_add_access("my-access"), 0);

    amxd_object_delete(&my_access);
}


void test_httpaccess_firewall_add_access_rules_with_access_allowing_all_ipv4_disable_and_allowed_source_prefix_empty(UNUSED void** state) {
    amxd_object_t* my_access = NULL;

    my_access = create_access_instance("my-access",
                                       true,
                                       "RemoteAccess",
                                       8080,
                                       "Device.IP.Interface.1",
                                       4,
                                       false,
                                       "",
                                       true,
                                       "");


    will_return_count(__wrap_amxm_execute_function, 0, 1);
    check_expected_firewall_service("tr181-httpaccess-Device-IP-Interface-1-v4-8080",
                                    "Device.IP.Interface.1", 8080, 4, "");

    assert_int_equal(httpaccess_firewall_add_access("my-access"), 0);

    amxd_object_delete(&my_access);
}

void test_httpaccess_firewall_add_access_rules_with_access_allowing_all_ipv6_disable_and_allowed_source_prefix_empty(UNUSED void** state) {
    amxd_object_t* my_access = NULL;

    my_access = create_access_instance("my-access",
                                       true,
                                       "RemoteAccess",
                                       8080,
                                       "Device.IP.Interface.1",
                                       6,
                                       true,
                                       "",
                                       false,
                                       "");

    will_return_count(__wrap_amxm_execute_function, 0, 1);
    check_expected_firewall_service("tr181-httpaccess-Device-IP-Interface-1-v6-8080",
                                    "Device.IP.Interface.1", 8080, 6, "");

    assert_int_equal(httpaccess_firewall_add_access("my-access"), 0);

    amxd_object_delete(&my_access);
}
