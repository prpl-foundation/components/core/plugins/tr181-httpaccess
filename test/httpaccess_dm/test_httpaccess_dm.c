/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdlib.h>
#include <setjmp.h>
#include <stdarg.h>
#include <cmocka.h>
#include <string.h>

#include <amxc/amxc.h>
#include <amxp/amxp.h>
#include <amxd/amxd_dm.h>
#include <amxd/amxd_object.h>
#include <amxd/amxd_transaction.h>
#include <amxo/amxo.h>
#include <amxb/amxb.h>

#include "dm_tr181-httpaccess.h"
#include "tr181-httpaccess.h"
#include "httpaccess_config.h"
#include "httpaccess_access.h"
#include "httpaccess_server.h"

#include "test_httpaccess_dm.h"


static amxd_dm_t dm;
static amxo_parser_t parser;
static const char* odl_defs = "httpaccess_test.odl";

static void handle_events(void) {
    while(amxp_signal_read() == 0) {
    }
}

static amxd_object_t* get_httpaccess_object() {
    amxd_object_t* httpaccess_object = NULL;

    httpaccess_object = amxd_dm_findf(httpaccess_get_dm(), HTTPACCESS_TR181_DEVICE_USERINTERFACE_HTTPACCESS_PATH);
    assert_non_null(httpaccess_object);

    return httpaccess_object;
}

int __wrap_amxm_so_open(UNUSED amxm_shared_object_t** so, UNUSED const char* shared_object_name, UNUSED const char* const path_to_so) {
    return 0;
}

int __wrap_amxm_execute_function(UNUSED const char* const shared_object_name,
                                 UNUSED const char* const module_name,
                                 const char* const func_name,
                                 amxc_var_t* args,
                                 amxc_var_t* ret) {
    if(strcmp(func_name, "update-accesses") == 0) {
        amxc_var_set_type(ret, AMXC_VAR_ID_HTABLE);
        amxc_var_for_each(config, args) {
            amxc_var_add_key(bool, ret, GETP_CHAR(config, "Alias"), true);
        }
    }
    return 0;
}

netmodel_query_t* __wrap_netmodel_openQuery_getAddrs(UNUSED const char* intf,
                                                     UNUSED const char* subscriber,
                                                     UNUSED const char* flag,
                                                     UNUSED const char* traverse,
                                                     netmodel_callback_t handler,
                                                     void* userdata) {
    amxc_var_t params, list;

    if(!intf || !*intf) {
        return NULL;
    }
    amxc_var_init(&params);
    amxc_var_set_type(&params, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &params, "Family", "ipv4");
    amxc_var_add_key(cstring_t, &params, "Address", "192.168.1.254");
    amxc_var_init(&list);
    amxc_var_set_type(&list, AMXC_VAR_ID_LIST);
    amxc_var_add(amxc_htable_t, &list, amxc_var_constcast(amxc_htable_t, &params));
    amxc_var_clean(&params);
    amxc_var_init(&params);
    amxc_var_set_type(&params, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &params, "Family", "ipv6");
    amxc_var_add_key(cstring_t, &params, "Address", "2041:0000:140F::875B:131B");
    amxc_var_add(amxc_htable_t, &list, amxc_var_constcast(amxc_htable_t, &params));
    amxc_var_clean(&params);
    handler(NULL, &list, userdata);
    amxc_var_clean(&list);
    return (netmodel_query_t*) 1;
}

void __wrap_netmodel_closeQuery(UNUSED netmodel_query_t* query) {
}

int test_httpaccess_setup(UNUSED void** state) {
    amxd_object_t* root_obj = NULL;

    assert_int_equal(amxd_dm_init(&dm), amxd_status_ok);
    assert_int_equal(amxo_parser_init(&parser), 0);

    root_obj = amxd_dm_get_root(&dm);
    assert_non_null(root_obj);

    assert_int_equal(amxo_resolver_ftab_add(&parser, "dm_httpaccess_changed",
                                            AMXO_FUNC(_dm_httpaccess_changed)), 0);
    assert_int_equal(amxo_resolver_ftab_add(&parser, "dm_httpaccess_added",
                                            AMXO_FUNC(_dm_httpaccess_added)), 0);
    assert_int_equal(amxo_resolver_ftab_add(&parser, "dm_httpaccess_destroyed",
                                            AMXO_FUNC(_dm_httpaccess_destroyed)), 0);
    assert_int_equal(amxo_resolver_ftab_add(&parser, "dm_httpaccess_ip_filtering_changed",
                                            AMXO_FUNC(_dm_httpaccess_ip_filtering_changed)), 0);
    assert_int_equal(amxo_parser_parse_file(&parser, odl_defs, root_obj), 0);

    _httpaccess_main(0, &dm, &parser);

    handle_events();

    return 0;
}

int test_httpaccess_teardown(UNUSED void** state) {
    _httpaccess_main(1, &dm, &parser);

    amxo_parser_clean(&parser);
    amxd_dm_clean(&dm);

    return 0;
}

void test_httpaccess_dm_changed(UNUSED void** state) {
    amxd_object_t* accesses = get_httpaccess_object();
    amxd_object_t* access;
    amxd_trans_t* trans;

    access = amxd_object_get_instance(accesses, NULL, 1);
    amxd_trans_new(&trans);
    amxd_trans_select_object(trans, access);
    amxd_trans_set_cstring_t(trans, "AllowedRoles", "Device.Users.Role.webui-role.");
    amxd_trans_set_cstring_t(trans, "Interface", "Device.IP.Interface.2.");
    amxd_trans_apply(trans, httpaccess_get_dm());
    amxd_trans_delete(&trans);
    handle_events();

    amxd_trans_new(&trans);
    amxd_trans_select_object(trans, access);
    amxd_trans_set_bool(trans, "Enable", false);
    amxd_trans_apply(trans, httpaccess_get_dm());
    amxd_trans_delete(&trans);
    handle_events();

    amxd_trans_new(&trans);
    amxd_trans_select_object(trans, access);
    amxd_trans_set_bool(trans, "Enable", true);
    amxd_trans_apply(trans, httpaccess_get_dm());
    amxd_trans_delete(&trans);
    handle_events();

    amxd_trans_new(&trans);
    amxd_trans_select_object(trans, access);
    amxd_trans_set_uint8_t(trans, "IPVersion", 0);
    amxd_trans_apply(trans, httpaccess_get_dm());
    amxd_trans_delete(&trans);
    handle_events();

    amxd_trans_new(&trans);
    amxd_trans_select_object(trans, access);
    amxd_trans_set_uint8_t(trans, "IPVersion", 4);
    amxd_trans_apply(trans, httpaccess_get_dm());
    amxd_trans_delete(&trans);
    handle_events();

    amxd_trans_new(&trans);
    amxd_trans_select_object(trans, access);
    amxd_trans_set_uint8_t(trans, "IPVersion", 6);
    amxd_trans_apply(trans, httpaccess_get_dm());
    amxd_trans_delete(&trans);
    handle_events();

    amxd_trans_new(&trans);
    amxd_trans_select_object(trans, access);
    amxd_trans_set_cstring_t(trans, "AllowedRoles", "Device.Users.Role.admin-role.");
    amxd_trans_apply(trans, httpaccess_get_dm());
    amxd_trans_delete(&trans);
    handle_events();
}

void test_httpaccess_dm_httpaccess_ip_filtering_changed(UNUSED void** state) {
    amxd_object_t* accesses = get_httpaccess_object();
    amxd_object_t* access;
    amxd_trans_t* trans;

    access = amxd_object_get_instance(accesses, NULL, 1);
    amxd_trans_new(&trans);
    amxd_trans_select_object(trans, access);
    amxd_trans_set_bool(trans, "AllowAllIPv4", "true");
    amxd_trans_set_bool(trans, "IPv4AllowedSourcePrefix", "plop1, plop2, plop3");
    amxd_trans_set_bool(trans, "AllowAllIPv6", "true");
    amxd_trans_set_bool(trans, "IPv6AllowedSourcePrefix", "plop1, plop2, plop3");
    amxd_trans_apply(trans, httpaccess_get_dm());
    amxd_trans_delete(&trans);
    handle_events();

    access = amxd_object_get_instance(accesses, NULL, 1);
    amxd_trans_new(&trans);
    amxd_trans_select_object(trans, access);
    amxd_trans_set_bool(trans, "AllowAllIPv4", "false");
    amxd_trans_set_bool(trans, "IPv4AllowedSourcePrefix", "plop1, plop2, plop3");
    amxd_trans_set_bool(trans, "AllowAllIPv6", "false");
    amxd_trans_set_bool(trans, "IPv6AllowedSourcePrefix", "plop1, plop2, plop3");
    amxd_trans_apply(trans, httpaccess_get_dm());
    amxd_trans_delete(&trans);
    handle_events();
}

void test_httpaccess_dm_deleted(UNUSED void** state) {
    amxd_object_t* accesses = get_httpaccess_object();
    amxd_trans_t* trans;

    amxd_trans_new(&trans);
    amxd_trans_select_object(trans, accesses);
    amxd_trans_del_inst(trans, 1, NULL);
    amxd_trans_apply(trans, httpaccess_get_dm());
    amxd_trans_delete(&trans);
    handle_events();

    _dm_httpaccess_destroyed(NULL, NULL, action_object_describe, NULL, NULL, NULL);
}
