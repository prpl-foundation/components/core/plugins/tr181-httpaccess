/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdlib.h>
#include <stdio.h>
#include <setjmp.h>
#include <stdarg.h>
#include <cmocka.h>
#include <string.h>
#include <signal.h>
#include <sys/signalfd.h>

#include <amxc/amxc.h>
#include <amxp/amxp.h>
#include <amxd/amxd_dm.h>
#include <amxd/amxd_object.h>
#include <amxd/amxd_transaction.h>
#include <amxo/amxo.h>
#include <amxb/amxb.h>
#include <yajl/yajl_gen.h>
#include <amxj/amxj_variant.h>
#include <amxut/amxut_bus.h>

#include "dm_tr181-httpaccess.h"
#include "dm_userinterface.h"
#include "tr181-httpaccess.h"
#include "httpaccess_session.h"

#include "test_httpaccess_session.h"

static const char* odl_defs = "httpaccess_test.odl";

static amxd_status_t test_check_credentials(UNUSED amxd_object_t* object,
                                            UNUSED amxd_function_t* func,
                                            amxc_var_t* args,
                                            UNUSED amxc_var_t* ret) {
    const char* username = GET_CHAR(args, "Username");
    const char* passwd = GET_CHAR(args, "Password");

    if((username != NULL) && (passwd != NULL) &&
       ((strcmp(username, "admin") == 0)) &&
       (strcmp(passwd, "admin") == 0)) {
        amxc_var_add_key(cstring_t, args, "Status", "Credentials_Good");
    } else {
        amxc_var_add_key(cstring_t, args, "Status", "Credentials_Bad");
    }
    return amxd_status_ok;
}


int test_httpaccess_setup(void** state) {
    amxut_bus_setup(state);

    assert_int_equal(amxo_resolver_ftab_add(amxut_bus_parser(), "dm_httpaccess_changed",
                                            AMXO_FUNC(_dm_httpaccess_changed)), 0);
    assert_int_equal(amxo_resolver_ftab_add(amxut_bus_parser(), "dm_httpaccess_added",
                                            AMXO_FUNC(_dm_httpaccess_added)), 0);
    assert_int_equal(amxo_resolver_ftab_add(amxut_bus_parser(), "dm_httpaccess_destroyed",
                                            AMXO_FUNC(_dm_httpaccess_destroyed)), 0);
    assert_int_equal(amxo_resolver_ftab_add(amxut_bus_parser(), "dm_httpaccess_session_added",
                                            AMXO_FUNC(_dm_httpaccess_session_added)), 0);
    assert_int_equal(amxo_resolver_ftab_add(amxut_bus_parser(), "dm_httpaccess_session_destroyed",
                                            AMXO_FUNC(_dm_httpaccess_session_destroyed)), 0);
    assert_int_equal(amxo_resolver_ftab_add(amxut_bus_parser(), "CheckCredentialsDiagnostics",
                                            AMXO_FUNC(test_check_credentials)), 0);
    assert_int_equal(amxo_resolver_ftab_add(amxut_bus_parser(), "CreateWebSession",
                                            AMXO_FUNC(_CreateWebSession)), 0);
    assert_int_equal(amxo_parser_parse_file(amxut_bus_parser(), odl_defs, amxd_dm_get_root(amxut_bus_dm())), 0);

    _httpaccess_main(0, amxut_bus_dm(), amxut_bus_parser());
    amxut_bus_handle_events();
    return 0;
}

int test_httpaccess_teardown(void** state) {
    _httpaccess_main(1, amxut_bus_dm(), amxut_bus_parser());
    return amxut_bus_teardown(state);
}

void test_httpaccess_session_check_creds_for_access(UNUSED void** state) {
    amxc_var_t args;
    amxc_var_t ret;

    amxc_var_init(&args);
    amxc_var_init(&ret);
    // Wrongly formatted input (no username)
    amxc_var_set(jstring_t, &args, "{\"password\":\"admin\",\"httpaccess\":\"UserInterface.HTTPAccess.1.\"}");
    amxc_var_cast(&args, AMXC_VAR_ID_ANY);
    assert_int_equal(_CheckCredentialsForAccess(NULL, NULL, &args, &ret), 0);
    assert_false(GET_BOOL(&ret, "Allowed"));
    // Wrongly formatted input (no password)
    amxc_var_set(jstring_t, &args, "{\"username\":\"admin\",\"httpaccess\":\"UserInterface.HTTPAccess.1.\"}");
    amxc_var_cast(&args, AMXC_VAR_ID_ANY);
    assert_int_equal(_CheckCredentialsForAccess(NULL, NULL, &args, &ret), 0);
    assert_false(GET_BOOL(&ret, "Allowed"));
    // Wrongly formatted input (no httpaccess)
    amxc_var_set(jstring_t, &args, "{\"username\":\"admin\",\"password\":\"admin\"}");
    amxc_var_cast(&args, AMXC_VAR_ID_ANY);
    assert_int_equal(_CheckCredentialsForAccess(NULL, NULL, &args, &ret), 0);
    assert_false(GET_BOOL(&ret, "Allowed"));
    // Wrongly formatted input (unexisting access)
    amxc_var_set(jstring_t, &args, "{\"username\":\"admina\",\"password\":\"admin\",\"httpaccess\":\"UserInterface.HTTPAccess.3.\"}");
    amxc_var_cast(&args, AMXC_VAR_ID_ANY);
    assert_int_equal(_CheckCredentialsForAccess(NULL, NULL, &args, &ret), 0);
    assert_false(GET_BOOL(&ret, "Allowed"));
    // Valid credentials check
    amxc_var_set(jstring_t, &args, "{\"username\":\"admin\",\"password\":\"admin\",\"httpaccess\":\"UserInterface.HTTPAccess.1.\"}");
    amxc_var_cast(&args, AMXC_VAR_ID_ANY);
    assert_int_equal(_CheckCredentialsForAccess(NULL, NULL, &args, &ret), 0);
    assert_true(GET_BOOL(&ret, "Allowed"));
    // Invalid credentials check
    amxc_var_set(jstring_t, &args, "{\"username\":\"admin\",\"password\":\"admina\",\"httpaccess\":\"UserInterface.HTTPAccess.1.\"}");
    amxc_var_cast(&args, AMXC_VAR_ID_ANY);
    assert_int_equal(_CheckCredentialsForAccess(NULL, NULL, &args, &ret), 0);
    assert_false(GET_BOOL(&ret, "Allowed"));
    // Valid credentials check but wrong role
    amxc_var_set(jstring_t, &args, "{\"username\":\"admin\",\"password\":\"admin\",\"httpaccess\":\"UserInterface.HTTPAccess.2.\"}");
    amxc_var_cast(&args, AMXC_VAR_ID_ANY);
    assert_int_equal(_CheckCredentialsForAccess(NULL, NULL, &args, &ret), 0);
    assert_false(GET_BOOL(&ret, "Allowed"));
    amxc_var_clean(&args);
    amxc_var_clean(&ret);
}

void test_httpaccess_session_create_websession(UNUSED void** state) {
    amxc_var_t args;
    amxc_var_t ret;
    amxd_object_t* sessions;

    amxc_var_init(&args);
    amxc_var_init(&ret);
    sessions = amxd_dm_findf(httpaccess_get_dm(), "UserInterface.HTTPAccess.1.Session.");
    assert_non_null(sessions);
    assert_int_equal(amxd_object_get_child_count(sessions), 0);
    amxc_var_set(jstring_t, &args, "{\"user\":\"Users.User.1.\", \"ip\":\"192.168.1.10\", \"port\":23456, \"protocol\":\"HTTP\", \"absolute_timeout\":10,\"idle_timeout\":2}");
    amxc_var_cast(&args, AMXC_VAR_ID_ANY);
    assert_int_equal(_CreateWebSession(amxd_dm_findf(httpaccess_get_dm(), "UserInterface.HTTPAccess.1."), NULL, &args, &ret), 0);
    assert_non_null(GET_ARG(&ret, "session_id"));
    amxut_bus_handle_events();
    sessions = amxd_dm_findf(httpaccess_get_dm(), "UserInterface.HTTPAccess.1.Session.[ID=='%s'].", GET_CHAR(&ret, "session_id"));
    assert_string_equal(GET_CHAR(amxd_object_get_param_value(sessions, "ID"), NULL), GET_CHAR(&ret, "session_id"));
    amxut_bus_handle_events();
    amxc_var_clean(&args);
    amxc_var_clean(&ret);
}
