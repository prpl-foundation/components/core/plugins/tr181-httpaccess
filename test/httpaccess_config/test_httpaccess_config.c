/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdlib.h>
#include <setjmp.h>
#include <stdarg.h>
#include <cmocka.h>
#include <string.h>

#include <amxc/amxc.h>
#include <amxp/amxp.h>
#include <amxd/amxd_dm.h>
#include <amxd/amxd_object.h>
#include <amxd/amxd_transaction.h>
#include <amxo/amxo.h>
#include <amxb/amxb.h>

#include "dm_tr181-httpaccess.h"
#include "tr181-httpaccess.h"
#include "httpaccess_config.h"
#include "httpaccess_access.h"

#include "test_httpaccess_config.h"


static amxd_dm_t dm;
static amxo_parser_t parser;
static const char* odl_defs = "httpaccess_test.odl";

static void handle_events(void) {
    while(amxp_signal_read() == 0) {
    }
}

static amxd_object_t* get_httpaccess_object() {
    amxd_object_t* httpaccess_object = NULL;

    httpaccess_object = amxd_dm_findf(httpaccess_get_dm(), HTTPACCESS_TR181_DEVICE_USERINTERFACE_HTTPACCESS_PATH);
    assert_non_null(httpaccess_object);

    return httpaccess_object;
}

static amxd_object_t* create_dummy_access_object(amxc_var_t* params) {
    amxd_object_t* dummy;

    amxd_object_new_instance(&dummy, get_httpaccess_object(), GET_CHAR(params, "Alias"), 0, params);
    amxd_object_set_cstring_t(dummy, "Alias", GET_CHAR(params, "Alias"));
    amxd_object_set_cstring_t(dummy, "AccessType", GET_CHAR(params, "AccessType"));
    return dummy;
}

static void add_state_to_dummy(amxd_object_t* dummy) {
    httpaccess_state_t* state;

    state = calloc(1, sizeof(*state));
    dummy->priv = state;
}

static void add_ipv4_address_to_dummy(amxd_object_t* dummy) {
    amxc_var_t* ip;
    httpaccess_state_t* state;

    state = dummy->priv;
    assert_non_null(state);
    amxc_var_new(&ip);
    amxc_var_set_type(ip, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, ip, "Address", "192.168.1.254");
    state->ipv4_info = ip;
}

static void add_ipv6_address_to_dummy(amxd_object_t* dummy, bool with_gua_address) {
    amxc_var_t* ipv6_list, * gua_address, * lla_address;
    httpaccess_state_t* state;

    state = dummy->priv;
    assert_non_null(state);

    amxc_var_new(&ipv6_list);
    amxc_var_set_type(ipv6_list, AMXC_VAR_ID_LIST);
    lla_address = amxc_var_add_new(ipv6_list);
    amxc_var_set_type(lla_address, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, lla_address, "Address", "fe80::2e93:fbff:fe15:72a1");
    amxc_var_add_key(cstring_t, lla_address, "TypeFlags", "@lla");

    if(with_gua_address) {
        gua_address = amxc_var_add_new(ipv6_list);
        amxc_var_set_type(gua_address, AMXC_VAR_ID_HTABLE);
        amxc_var_add_key(cstring_t, gua_address, "Address", "2041:0000:140F::875B:131B");
        amxc_var_add_key(cstring_t, gua_address, "TypeFlags", "@gua");
    }

    state->ipv6_info_list = ipv6_list;
}


int __wrap_amxm_so_open(UNUSED amxm_shared_object_t** so, UNUSED const char* shared_object_name, UNUSED const char* const path_to_so) {
    return 0;
}

int test_httpaccess_setup(UNUSED void** state) {
    amxd_object_t* root_obj = NULL;

    assert_int_equal(amxd_dm_init(&dm), amxd_status_ok);
    assert_int_equal(amxo_parser_init(&parser), 0);

    root_obj = amxd_dm_get_root(&dm);
    assert_non_null(root_obj);

    assert_int_equal(amxo_resolver_ftab_add(&parser, "dm_httpaccess_changed",
                                            AMXO_FUNC(_dm_httpaccess_changed)), 0);
    assert_int_equal(amxo_resolver_ftab_add(&parser, "dm_httpaccess_added",
                                            AMXO_FUNC(_dm_httpaccess_added)), 0);
    assert_int_equal(amxo_resolver_ftab_add(&parser, "dm_httpaccess_destroyed",
                                            AMXO_FUNC(_dm_httpaccess_destroyed)), 0);
    assert_int_equal(amxo_parser_parse_file(&parser, odl_defs, root_obj), 0);

    _httpaccess_main(0, &dm, &parser);

    handle_events();

    return 0;
}

int test_httpaccess_teardown(UNUSED void** state) {
    _httpaccess_main(1, &dm, &parser);

    amxo_parser_clean(&parser);
    amxd_dm_clean(&dm);

    return 0;
}

void test_httpaccess_config_update(UNUSED void** state) {
    amxc_var_t params, config;
    amxd_object_t* dummy_access;


    assert_int_equal(httpaccess_config_update(NULL, NULL), -1);

    amxc_var_init(&config);
    assert_int_equal(httpaccess_config_update(NULL, &config), -1);

    amxc_var_init(&params);
    amxc_var_set_type(&params, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &params, "Alias", "dummy-access");
    amxc_var_add_key(bool, &params, "Enable", true);
    amxc_var_add_key(cstring_t, &params, "AccessType", "RemoteAccess");
    dummy_access = create_dummy_access_object(&params);
    assert_int_equal(httpaccess_config_update(dummy_access, NULL), -1);

    //No state
    assert_int_equal(httpaccess_config_update(dummy_access, &config), -1);

    // No IP
    add_state_to_dummy(dummy_access);
    assert_int_equal(httpaccess_config_update(dummy_access, &config), -1);
    assert_string_equal(HTTPACCESS_GET_ACCESS_PARAM(cstring_t, dummy_access, "Status"), "Down");

    //OK
    add_ipv4_address_to_dummy(dummy_access);
    add_ipv6_address_to_dummy(dummy_access, true);
    assert_int_equal(httpaccess_config_update(dummy_access, &config), 0);
    assert_string_not_equal(HTTPACCESS_GET_ACCESS_PARAM(cstring_t, dummy_access, "Status"), "Error");

    assert_string_equal(GET_CHAR(&config, "Alias"), "dummy-access");
    assert_string_equal(GETP_CHAR(&config, "IPv4.Address"), "192.168.1.254");
    assert_string_equal(GETP_CHAR(&config, "IPv6_list.0.Address"), "fe80::2e93:fbff:fe15:72a1");
    assert_string_equal(GETP_CHAR(&config, "IPv6_list.0.TypeFlags"), "@lla");
    assert_string_equal(GETP_CHAR(&config, "IPv6_list.1.Address"), "2041:0000:140F::875B:131B");
    assert_string_equal(GETP_CHAR(&config, "IPv6_list.1.TypeFlags"), "@gua");

    amxd_object_delete(&dummy_access);
    amxc_var_clean(&params);
    amxc_var_clean(&config);
}

void test_httpaccess_config_update_with_no_ipv6_gua_address(UNUSED void** state) {
    amxc_var_t params, config;
    amxd_object_t* dummy_access;


    assert_int_equal(httpaccess_config_update(NULL, NULL), -1);

    amxc_var_init(&config);
    assert_int_equal(httpaccess_config_update(NULL, &config), -1);

    amxc_var_init(&params);
    amxc_var_set_type(&params, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &params, "Alias", "dummy-access");
    amxc_var_add_key(bool, &params, "Enable", true);
    amxc_var_add_key(cstring_t, &params, "AccessType", "LocalAccess");
    dummy_access = create_dummy_access_object(&params);
    assert_int_equal(httpaccess_config_update(dummy_access, NULL), -1);

    //No state
    assert_int_equal(httpaccess_config_update(dummy_access, &config), -1);

    // No IP
    add_state_to_dummy(dummy_access);
    assert_int_equal(httpaccess_config_update(dummy_access, &config), -1);
    assert_string_equal(HTTPACCESS_GET_ACCESS_PARAM(cstring_t, dummy_access, "Status"), "Down");

    //OK
    add_ipv4_address_to_dummy(dummy_access);
    add_ipv6_address_to_dummy(dummy_access, false);
    assert_int_equal(httpaccess_config_update(dummy_access, &config), 0);
    assert_string_not_equal(HTTPACCESS_GET_ACCESS_PARAM(cstring_t, dummy_access, "Status"), "Error");

    assert_string_equal(GET_CHAR(&config, "Alias"), "dummy-access");
    assert_string_equal(GETP_CHAR(&config, "IPv6_list.0.Address"), "fe80::2e93:fbff:fe15:72a1");
    assert_string_equal(GETP_CHAR(&config, "IPv6_list.0.TypeFlags"), "@lla");

    amxd_object_delete(&dummy_access);
    amxc_var_clean(&params);
    amxc_var_clean(&config);
}

void test_httpaccess_config_delete(UNUSED void** state) {
    amxc_var_t config;

    amxc_var_init(&config);
    assert_int_equal(httpaccess_config_delete(NULL, NULL), -1);
    assert_int_equal(httpaccess_config_delete("dummy-access", NULL), -1);
    assert_int_equal(httpaccess_config_delete(NULL, &config), -1);
    assert_int_equal(httpaccess_config_delete("dummy-access", &config), 0);
    assert_string_equal(GET_CHAR(&config, "Alias"), "dummy-access");
    amxc_var_clean(&config);
}