/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <setjmp.h>
#include <stdarg.h>
#include <string.h>

#include <cmocka.h>
#include <amxut/amxut_bus.h>

#include <amxc/amxc.h>
#include <amxc/amxc_macros.h>
#include <amxp/amxp.h>
#include <amxd/amxd_dm.h>
#include <amxd/amxd_object.h>

#include "test_userinterface.h"
#include "test_credentials.h"

static void password_user_selectable_set(bool selectable) {
    amxc_var_t values, ret;

    amxc_var_init(&ret);
    amxc_var_init(&values);
    amxc_var_set_type(&values, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(bool, &values, "PasswordUserSelectable", selectable);

    assert_int_equal(amxb_set(amxb_be_who_has("UserInterface"), "UserInterface", &values, &ret, 5), 0);

    amxc_var_clean(&values);
    amxc_var_clean(&ret);
}

static void call_check_credentials_for_access(const char* username, const char* password, const char* interface, bool expected_allowed, int expected_login_attempts) {
    amxc_var_t args, ret;

    amxc_var_init(&ret);
    amxc_var_init(&args);
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "username", username);
    amxc_var_add_key(cstring_t, &args, "password", password);
    amxc_var_add_key(cstring_t, &args, "httpaccess", interface);

    assert_int_equal(amxb_call(amxb_be_who_has("UserInterface"), "UserInterface", "CheckCredentialsForAccess", &args, &ret, 5), 0);

    assert_int_equal(GET_BOOL(amxc_var_get_first(&ret), "Allowed"), expected_allowed);

    if(expected_login_attempts >= 0) {
        assert_int_equal(GET_UINT32(amxc_var_get_first(&ret), "LoginAttempts"), expected_login_attempts);
    }

    amxc_var_clean(&args);
    amxc_var_clean(&ret);
}

void test_credentials_check_user_password(UNUSED void** state) {
    expect_string(_CheckCredentialsDiagnostics, username, "admin");
    expect_string(_CheckCredentialsDiagnostics, password, "admin");
    will_return(_CheckCredentialsDiagnostics, "Credentials_Good");
    password_user_selectable_set(true);
    call_check_credentials_for_access("admin", "admin", "UserInterface.HTTPAccess.1.", true, 5);
}

void test_credentials_check_user_with_wrong_password(UNUSED void** state) {
    expect_string(_CheckCredentialsDiagnostics, username, "admin");
    expect_string(_CheckCredentialsDiagnostics, password, "wrong");
    will_return(_CheckCredentialsDiagnostics, "Credentials_Bad_Requested_Password_Incorrect");
    password_user_selectable_set(true);
    call_check_credentials_for_access("admin", "wrong", "UserInterface.HTTPAccess.1.", false, -1);
}

void test_credentials_check_invalid_username(UNUSED void** state) {
    password_user_selectable_set(true);
    call_check_credentials_for_access("", "admin", "UserInterface.HTTPAccess.1.", false, -1);
}

void test_credentials_lan_auto_configurator_can_overrule_user_password(UNUSED void** state) {
    password_user_selectable_set(false);
    call_check_credentials_for_access("admin", "ConfigSecret", "UserInterface.HTTPAccess.1.", true, 5);
}
