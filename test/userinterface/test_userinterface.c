/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <setjmp.h>
#include <stdarg.h>
#include <string.h>

#include <cmocka.h>
#include <amxut/amxut_bus.h>
#include <amxut/amxut_sahtrace.h>

#include <amxc/amxc.h>
#include <amxd/amxd_dm.h>
#include <amxd/amxd_object.h>
#include <debug/sahtrace.h>

#include "dm_tr181-httpaccess.h"
#include "tr181-httpaccess.h"
#include "httpaccess_access.h"
#include "dm_userinterface.h"

#include "mock_users.h"
#include "test_userinterface.h"

#define dm_resolve_rpc(func) assert_int_equal(amxo_resolver_ftab_add(amxut_bus_parser(), #func, AMXO_FUNC(_ ## func)), 0);

static void resolve_dm_functions(void) {
    // UserInterface.
    dm_resolve_rpc(PasswordReset);
    dm_resolve_rpc(CheckCredentialsForAccess);

    // UserInterface.HTTPAccess.
    dm_resolve_rpc(dm_httpaccess_added);
    dm_resolve_rpc(dm_httpaccess_changed);
    dm_resolve_rpc(dm_httpaccess_destroyed);
    dm_resolve_rpc(dm_httpaccess_session_added);
    dm_resolve_rpc(dm_httpaccess_session_destroyed);

    // UserInterface.HTTPAccess.Session.
    dm_resolve_rpc(CreateWebSession);
    dm_resolve_rpc(CheckSessionValid);
    dm_resolve_rpc(Delete);

    // Users.
    dm_resolve_rpc(CheckCredentialsDiagnostics);
}

int test_userinterface_setup(UNUSED void** state) {
    assert_int_equal(amxut_bus_setup(state), 0);

    resolve_dm_functions();

    amxd_object_t* root_obj = amxd_dm_get_root(amxut_bus_dm());
    assert_non_null(root_obj);
    assert_int_equal(amxo_parser_parse_file(amxut_bus_parser(), "odl/userinterface_test.odl", root_obj), 0);

    _httpaccess_main(0, amxut_bus_dm(), amxut_bus_parser());

    amxut_bus_handle_events();

    return 0;
}

int test_userinterface_teardown(UNUSED void** state) {
    _httpaccess_main(1, amxut_bus_dm(), amxut_bus_parser());

    amxut_bus_handle_events();

    assert_int_equal(amxut_bus_teardown(state), 0);
    return 0;
}
