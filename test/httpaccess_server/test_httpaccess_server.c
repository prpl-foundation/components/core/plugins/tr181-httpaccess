/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdlib.h>
#include <setjmp.h>
#include <stdarg.h>
#include <cmocka.h>
#include <string.h>

#include <amxc/amxc.h>
#include <amxp/amxp.h>
#include <amxd/amxd_dm.h>
#include <amxd/amxd_object.h>
#include <amxd/amxd_transaction.h>
#include <amxo/amxo.h>
#include <amxb/amxb.h>

#include "dm_tr181-httpaccess.h"
#include "tr181-httpaccess.h"
#include "httpaccess_config.h"
#include "httpaccess_access.h"
#include "httpaccess_server.h"

#include "test_httpaccess_server.h"

typedef struct config {
    uint8_t ip_version;
} config_t;

static config_t config = {
    .ip_version = 0
};

static amxd_dm_t dm;
static amxo_parser_t parser;
static const char* odl_defs = "httpaccess_test.odl";

static void handle_events(void) {
    while(amxp_signal_read() == 0) {
    }
}

static amxd_object_t* get_httpaccess_object() {
    amxd_object_t* httpaccess_object = NULL;

    httpaccess_object = amxd_dm_findf(httpaccess_get_dm(), HTTPACCESS_TR181_DEVICE_USERINTERFACE_HTTPACCESS_PATH);
    assert_non_null(httpaccess_object);

    return httpaccess_object;
}

static amxd_object_t* create_dummy_access_object(amxc_var_t* params) {
    amxd_object_t* dummy;

    amxd_object_new_instance(&dummy, get_httpaccess_object(), GET_CHAR(params, "Alias"), 0, params);
    amxd_object_set_cstring_t(dummy, "Alias", GET_CHAR(params, "Alias"));
    amxd_object_set_bool(dummy, "Enable", GET_BOOL(params, "Enable"));
    amxd_object_set_uint8_t(dummy, "IPVersion", GET_INT32(params, "IPVersion"));
    return dummy;
}

static void add_state_to_dummy(amxd_object_t* dummy) {
    httpaccess_state_t* state;

    state = calloc(1, sizeof(*state));
    dummy->priv = state;
}

static void add_ip_to_dummy(amxd_object_t* dummy) {
    amxc_var_t* ip, * ipv6_list, * gua_address, * lla_address;
    httpaccess_state_t* state;

    state = dummy->priv;
    assert_non_null(state);
    amxc_var_new(&ip);
    amxc_var_set_type(ip, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, ip, "Address", "192.168.1.254");
    state->ipv4_info = ip;

    amxc_var_new(&ipv6_list);
    amxc_var_set_type(ipv6_list, AMXC_VAR_ID_LIST);
    gua_address = amxc_var_add_new(ipv6_list);
    amxc_var_set_type(gua_address, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, gua_address, "Address", "2041:0000:140F::875B:131B");
    amxc_var_add_key(cstring_t, gua_address, "TypeFlags", "@gua");
    lla_address = amxc_var_add_new(ipv6_list);
    amxc_var_set_type(lla_address, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, lla_address, "Address", "fe80::2e93:fbff:fe15:72a1");
    amxc_var_add_key(cstring_t, lla_address, "TypeFlags", "@lla");

    state->ipv6_info_list = ipv6_list;
}

int __wrap_amxm_so_open(UNUSED amxm_shared_object_t** so, UNUSED const char* shared_object_name, UNUSED const char* const path_to_so) {
    return 0;
}

int __wrap_amxm_execute_function(UNUSED const char* const shared_object_name,
                                 UNUSED const char* const module_name,
                                 const char* const func_name,
                                 amxc_var_t* args,
                                 amxc_var_t* ret) {
    if(strcmp(func_name, "update-accesses") == 0) {
        amxc_var_set_type(ret, AMXC_VAR_ID_HTABLE);
        amxc_var_for_each(config, args) {
            const char* alias = GETP_CHAR(config, "Alias");
            amxc_var_add_key(bool, ret, alias, true);
            check_expected(alias);

            const char* IPv4Address = GETP_CHAR(config, "IPv4.Address");
            check_expected(IPv4Address);

            const char* IPv6AddressGUA = GETP_CHAR(config, "IPv6_list.0.Address");
            check_expected_ptr(IPv6AddressGUA);

            const char* IPv6AddressLLA = GETP_CHAR(config, "IPv6_list.1.Address");
            check_expected_ptr(IPv6AddressLLA);
        }
    }
    return 0;
}

static int test_httpaccess_setup(UNUSED void** state) {
    amxd_object_t* root_obj = NULL;

    printf("[ RUN      ] With IP Version = %u\n", ((config_t*) (*state))->ip_version);

    assert_int_equal(amxd_dm_init(&dm), amxd_status_ok);
    assert_int_equal(amxo_parser_init(&parser), 0);

    root_obj = amxd_dm_get_root(&dm);
    assert_non_null(root_obj);

    assert_int_equal(amxo_resolver_ftab_add(&parser, "dm_httpaccess_changed",
                                            AMXO_FUNC(_dm_httpaccess_changed)), 0);
    assert_int_equal(amxo_resolver_ftab_add(&parser, "dm_httpaccess_added",
                                            AMXO_FUNC(_dm_httpaccess_added)), 0);
    assert_int_equal(amxo_resolver_ftab_add(&parser, "dm_httpaccess_destroyed",
                                            AMXO_FUNC(_dm_httpaccess_destroyed)), 0);
    assert_int_equal(amxo_parser_parse_file(&parser, odl_defs, root_obj), 0);

    _httpaccess_main(0, &dm, &parser);

    handle_events();

    return 0;
}

int test_httpaccess_setup_ipversion_0(void** state) {
    config.ip_version = 0;
    (*state) = (void*) &config;
    return test_httpaccess_setup(state);
}

int test_httpaccess_setup_ipversion_4(void** state) {
    config.ip_version = 4;
    (*state) = (void*) &config;
    return test_httpaccess_setup(state);
}

int test_httpaccess_setup_ipversion_6(void** state) {
    config.ip_version = 6;
    (*state) = (void*) &config;
    return test_httpaccess_setup(state);
}

int test_httpaccess_teardown(UNUSED void** state) {
    _httpaccess_main(1, &dm, &parser);

    amxo_parser_clean(&parser);
    amxd_dm_clean(&dm);

    return 0;
}

static void check_expected_ip_adrresses(uint8_t ip_version) {

    if(ip_version == 4) {
        expect_string(__wrap_amxm_execute_function, IPv4Address, "192.168.1.254");
        expect_value(__wrap_amxm_execute_function, IPv6AddressGUA, NULL);
        expect_value(__wrap_amxm_execute_function, IPv6AddressLLA, NULL);

    } else if(ip_version == 6) {
        expect_value(__wrap_amxm_execute_function, IPv4Address, NULL);
        expect_string(__wrap_amxm_execute_function, IPv6AddressGUA, "2041:0000:140F::875B:131B");
        expect_string(__wrap_amxm_execute_function, IPv6AddressLLA, "fe80::2e93:fbff:fe15:72a1");

    } else {
        expect_string(__wrap_amxm_execute_function, IPv4Address, "192.168.1.254");
        expect_string(__wrap_amxm_execute_function, IPv6AddressGUA, "2041:0000:140F::875B:131B");
        expect_string(__wrap_amxm_execute_function, IPv6AddressLLA, "fe80::2e93:fbff:fe15:72a1");
    }
}

void test_httpaccess_server_apply_config(UNUSED void** state) {
    amxd_object_t* dummy_access;
    amxc_var_t params;
    config_t* config = ((config_t*) (*state));

    amxc_var_init(&params);
    amxc_var_set_type(&params, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &params, "Alias", "dummy-access");
    amxc_var_add_key(bool, &params, "Enable", true);
    amxc_var_add_key(uint8_t, &params, "IPVersion", config->ip_version);
    dummy_access = create_dummy_access_object(&params);
    amxc_var_clean(&params);

    assert_int_equal(httpaccess_server_update_apply_config(NULL), -1);

    add_state_to_dummy(dummy_access);
    add_ip_to_dummy(dummy_access);

    expect_string(__wrap_amxm_execute_function, alias, "dummy-access");

    check_expected_ip_adrresses(config->ip_version);

    assert_int_equal(httpaccess_server_update_apply_config(dummy_access), 0);

    amxd_object_delete(&dummy_access);
}

void test_httpaccess_server_apply_config_delay(UNUSED void** state) {
    amxd_object_t* dummy_access, * dummy_access1;
    amxc_var_t params;
    config_t* config = ((config_t*) (*state));

    amxc_var_init(&params);
    amxc_var_set_type(&params, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &params, "Alias", "dummy-access");
    amxc_var_add_key(bool, &params, "Enable", true);
    amxc_var_add_key(uint8_t, &params, "IPVersion", config->ip_version);
    dummy_access = create_dummy_access_object(&params);
    amxc_var_clean(&params);
    add_state_to_dummy(dummy_access);
    add_ip_to_dummy(dummy_access);

    amxc_var_add_key(uint32_t, httpaccess_get_config(), HTTPACCESS_APPLY_DELAY_CONFIG, 10000);

    assert_int_equal(httpaccess_server_update_apply_config(dummy_access), 0);

    amxc_var_init(&params);
    amxc_var_set_type(&params, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &params, "Alias", "dummy-access1");
    amxc_var_add_key(bool, &params, "Enable", true);
    dummy_access1 = create_dummy_access_object(&params);
    amxc_var_clean(&params);
    add_state_to_dummy(dummy_access1);
    add_ip_to_dummy(dummy_access1);
    assert_int_equal(httpaccess_server_update_apply_config(dummy_access1), 0);

    //Add duplicate
    assert_int_equal(httpaccess_server_update_apply_config(dummy_access), 0);

    amxd_object_delete(&dummy_access);
    amxd_object_delete(&dummy_access1);
    httpaccess_server_clean_configs();
    httpaccess_server_clean_timer();

}
