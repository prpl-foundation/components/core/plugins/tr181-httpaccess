/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <string.h>
#include <amxc/amxc.h>
#include <amxc/amxc_macros.h>
#include <amxp/amxp.h>
#include <amxd/amxd_object.h>
#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>
#include "tr181-httpaccess.h"
#include "httpaccess_config.h"
#include "httpaccess_access.h"

#define ME "config"

int httpaccess_config_check_proxy(amxc_var_t* proxy) {
    int retval = -1;
    const char* host = NULL;
    const char* prefix = NULL;

    when_null(proxy, exit);
    when_str_empty_trace(GET_CHAR(proxy, "ProxyIP"), exit,
                         WARNING, "Access is of type Proxy but ProxyIP is empty");
    host = GET_CHAR(proxy, "Hosts");
    prefix = GET_CHAR(proxy, "Prefixes");
    when_null(host, exit);
    when_null(prefix, exit);
    if((host[0] == '\0') && (prefix[0] == '\0')) {
        SAH_TRACEZ_WARNING(ME, "Access of type Proxy should have at least one host or one prefix configured");
        goto exit;
    }
    retval = 0;
exit:
    return retval;
}

static int httpaccess_config_proxy(amxd_object_t* access, amxc_var_t* config) {
    int retval = -1;

    amxc_var_add_key(cstring_t, config, "ProxyIP", HTTPACCESS_GET_ACCESS_PARAM(cstring_t, access, "ProxyIP"));
    amxc_var_add_key(uint32_t, config, "ProxyPort", HTTPACCESS_GET_ACCESS_PARAM(uint32_t, access, "ProxyPort"));
    when_failed(httpaccess_config_check_proxy(config), exit);
    retval = 0;
exit:
    return retval;
}

int httpaccess_config_check_vhost(amxc_var_t* vhost) {
    int retval = -1;
    const char* host = NULL;
    const char* prefix = NULL;

    when_null(vhost, exit);
    when_str_empty_trace(GET_CHAR(vhost, "VHostDocumentRoot"), exit,
                         WARNING, "Access is of type VHost but VHostDocumentRoot is empty");
    when_true_trace(strstr(GET_CHAR(vhost, "VHostDocumentRoot"), "..") != NULL, exit,
                    WARNING, "VHostDocumentRoot shouldn't contains \"..\" (%s)", GET_CHAR(vhost, "VHostDocumentRoot"));
    host = GET_CHAR(vhost, "Hosts");
    prefix = GET_CHAR(vhost, "Prefixes");
    when_null(host, exit);
    when_null(prefix, exit);
    if((host[0] == '\0') && (prefix[0] == '\0')) {
        SAH_TRACEZ_WARNING(ME, "Access of type VHost should have at least one host or one prefix configured");
        goto exit;
    }
    retval = 0;
exit:
    return retval;
}

static int httpaccess_config_vhost(UNUSED amxd_object_t* access, UNUSED amxc_var_t* config) {
    int retval = -1;

    amxc_var_add_key(cstring_t, config, "VHostDocumentRoot", HTTPACCESS_GET_ACCESS_PARAM(cstring_t, access, "VHostDocumentRoot"));
    when_failed(httpaccess_config_check_vhost(config), exit);
    retval = 0;
exit:
    return retval;
}

static int httpaccess_config_access_type(amxd_object_t* access, UNUSED amxc_var_t* config) {
    int retval = -1;
    amxd_object_t* http_config = amxd_object_get(access, "X_PRPL-COM_HTTPConfig");
    const char* type = NULL;

    when_null(http_config, exit);
    type = HTTPACCESS_GET_ACCESS_PARAM(cstring_t, http_config, "Type");
    amxc_var_add_key(cstring_t, config, "Type", type);
    if(strcmp(type, "Proxy") == 0) {
        when_failed(httpaccess_config_proxy(http_config, config), exit);
    } else if(strcmp(type, "VHost") == 0) {
        when_failed(httpaccess_config_vhost(http_config, config), exit);
    }
    retval = 0;
exit:
    return retval;
}

static void httpaccess_config_extra_conf(amxd_object_t* _access, amxc_var_t* config) {
    amxc_string_t filepath;

    amxc_string_init(&filepath, 0);
    when_str_empty(GET_CHAR(httpaccess_get_config(), HTTPACCESS_EXTRA_CONF_DIR_CONFIG), exit);
    amxc_string_setf(&filepath, "%s/%s.conf", GET_CHAR(httpaccess_get_config(), HTTPACCESS_EXTRA_CONF_DIR_CONFIG), HTTPACCESS_CONFIG_EXTRA_CONF_FILENAME_PREFIX);
    if(access(amxc_string_get(&filepath, 0), F_OK) == 0) {
        amxc_var_add_key(cstring_t, config, "extra-conf-default-file", amxc_string_get(&filepath, 0));
    }
    amxc_string_setf(&filepath, "%s/%s-%s.conf", GET_CHAR(httpaccess_get_config(), HTTPACCESS_EXTRA_CONF_DIR_CONFIG), HTTPACCESS_CONFIG_EXTRA_CONF_FILENAME_PREFIX, HTTPACCESS_GET_ACCESS_ALIAS(_access));
    if(access(amxc_string_get(&filepath, 0), F_OK) == 0) {
        amxc_var_add_key(cstring_t, config, "extra-conf-file", amxc_string_get(&filepath, 0));
    }
exit:
    amxc_string_clean(&filepath);
    return;
}

int httpaccess_config_update(amxd_object_t* access, amxc_var_t* config) {
    int retval = -1;
    httpaccess_state_t* state = NULL;
    uint8_t ipversion = 0;

    when_null(access, exit);
    when_null(access->priv, exit);
    when_null(config, exit);

    state = (httpaccess_state_t*) access->priv;
    when_false_status(state->ipv4_info || state->ipv6_info_list, exit, retval = -1);
    when_failed(amxc_var_set_type(config, AMXC_VAR_ID_HTABLE), exit);
    amxc_var_add_key(cstring_t, config, "Alias", HTTPACCESS_GET_ACCESS_ALIAS(access));
    amxc_var_add_key(uint32_t, config, "Port", HTTPACCESS_GET_ACCESS_PARAM(uint32_t, access, "Port"));
    amxc_var_add_key(uint32_t, config, "Order", HTTPACCESS_GET_ACCESS_PARAM(uint32_t, access, "Order"));
    amxc_var_add_key(cstring_t, config, "Protocol", HTTPACCESS_GET_ACCESS_PARAM(cstring_t, access, "Protocol"));
    amxc_var_add_key(cstring_t, config, "Prefixes", HTTPACCESS_GET_ACCESS_PARAM(cstring_t, access, "AllowedPathPrefix"));
    amxc_var_add_key(cstring_t, config, "Hosts", HTTPACCESS_GET_ACCESS_PARAM(cstring_t, access, "AllowedHosts"));
    when_true_status(httpaccess_config_access_type(access, config) != 0, exit, retval = -2);
    httpaccess_config_extra_conf(access, config);

    ipversion = HTTPACCESS_GET_ACCESS_PARAM(uint8_t, access, "IPVersion");
    if((state->ipv4_info != NULL) && ((ipversion == 0) || (ipversion == 4))) {
        amxc_var_add_key(amxc_htable_t, config, "IPv4", amxc_var_constcast(amxc_htable_t, state->ipv4_info));
    }

    if((state->ipv6_info_list != NULL) && ((ipversion == 0) || (ipversion == 6))) {
        amxc_var_add_key(amxc_llist_t, config, "IPv6_list", amxc_var_constcast(amxc_llist_t, state->ipv6_info_list));
    }

    retval = 0;
exit:
    if(retval == -2) {
        SAH_TRACEZ_ERROR(ME, "Can't create the variant for Access %s because it is missconfigured", HTTPACCESS_GET_ACCESS_ALIAS(access));
        httpaccess_access_set_status(HTTPACCESS_GET_ACCESS_ALIAS(access), "Error");
    }
    return retval;
}

int httpaccess_config_delete(const char* alias, amxc_var_t* config) {
    int retval = -1;

    when_str_empty(alias, exit);
    when_null(config, exit);
    when_failed(amxc_var_set_type(config, AMXC_VAR_ID_HTABLE), exit);

    amxc_var_add_key(cstring_t, config, "Alias", alias);

    retval = 0;
exit:
    return retval;
}
