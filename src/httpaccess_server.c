/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <string.h>
#include <amxc/amxc.h>
#include <amxc/amxc_macros.h>
#include <amxp/amxp.h>
#include <amxm/amxm.h>
#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>
#include "tr181-httpaccess.h"
#include "dm_tr181-httpaccess.h"
#include "httpaccess_config.h"
#include "httpaccess_mod.h"
#include "httpaccess_server.h"

#define ME "server"

static amxp_timer_t* update_timer = NULL;
static amxc_var_t* update_configs = NULL;
static uint8_t restart_time = 0x01;

inline void httpaccess_server_clean_timer(void) {
    amxp_timer_delete(&update_timer);
}

inline void httpaccess_server_clean_configs(void) {
    amxc_var_delete(&update_configs);
}

//GCOV_EXCL_START can't cover static amxp_timer_cb
static void httpaccess_server_apply_configs_to_server(UNUSED amxp_timer_t* timer, UNUSED void* priv) {
    SAH_TRACEZ_INFO(ME, "!!!!! Timer has expired apply configs");
    httpaccess_mod_apply_configs(update_configs);
    httpaccess_server_clean_configs();
    httpaccess_server_clean_timer();
}

static void httpaccess_server_restart_server(UNUSED amxp_timer_t* timer, UNUSED void* priv) {
    SAH_TRACEZ_INFO(ME, "!!!!! Timer has expired starting server");
    httpaccess_mod_start();
    httpaccess_server_clean_timer();
}
//GCOV_EXCL_STOP

static int httpaccess_server_stopped_unexpectedly(UNUSED const char* function_name,
                                                  UNUSED amxc_var_t* args,
                                                  UNUSED amxc_var_t* ret) {
    int r = 1;
    SAH_TRACEZ_ERROR(ME, "Back-end stopped unexpectedly, restarting it in %d second", restart_time);

    // make sure that only one timer is running
    if((update_timer != NULL) && (amxp_timer_remaining_time(update_timer) > 0)) {
        SAH_TRACEZ_WARNING(ME, "Update timer have %d ms, erasing it for backoff time", amxp_timer_remaining_time(update_timer));
        when_failed_trace(amxp_timer_start(update_timer, restart_time * 1000), exit, ERROR, "Failed to launch update timer, back-end will not restart");
    } else {
        when_failed(amxp_timer_new(&update_timer, httpaccess_server_restart_server, NULL), exit);
        when_failed_trace(amxp_timer_start(update_timer, restart_time * 1000), exit, ERROR, "Failed to launch restart timer, back-end will not restart");
    }

    if(restart_time >= 0x80) {
        SAH_TRACEZ_WARNING(ME, "Backoff time reach maximum, reseting it to 1 second");
        restart_time = 0x01;
    } else {
        restart_time <<= 1;
    }
    r = 0;
exit:
    return r;
}

int httpaccess_server_init(void) {
    amxm_shared_object_t* so = amxm_get_so("self");
    amxm_module_t* mod = NULL;
    int ret = 1;

    when_null_trace(so, exit, ERROR, "Unable to get shared object.");

    when_failed_trace(amxm_module_register(&mod, so, MOD_HTTPACCESS_CORE), exit, ERROR, "Cannot register core module");
    amxm_module_add_function(mod, "server-stopped-unexpectedly", httpaccess_server_stopped_unexpectedly);
    ret = 0;
exit:
    return ret;
}

static void httpaccess_server_remove_existing_config(const char* alias) {
    unsigned int i = 0;

    when_str_empty(alias, exit);
    amxc_var_for_each(conf, update_configs) {
        if(strcmp(GET_CHAR(conf, "Alias"), alias) == 0) {
            amxc_var_t* tmp = amxc_var_take_index(update_configs, i);
            amxc_var_delete(&tmp);
            break;
        }
        ++i;
    }
exit:
    return;
}

static int httpaccess_server_add_config_to_configs(amxc_var_t* config) {
    int retval = -1;

    when_null(config, exit);
    if(update_configs == NULL) {
        when_failed(amxc_var_new(&update_configs), exit);
        when_failed(amxc_var_set_type(update_configs, AMXC_VAR_ID_LIST), exit);
    }
    httpaccess_server_remove_existing_config(GET_CHAR(config, "Alias"));
    amxc_var_add(amxc_htable_t, update_configs, amxc_var_constcast(amxc_htable_t, config));
    retval = 0;
exit:
    return retval;
}

static int httpaccess_server_apply_configs(void) {
    int retval = -1;

    when_null(update_configs, exit);
    if(httpaccess_get_apply_delay() != 0) {
        if(update_timer != NULL) {
            httpaccess_server_clean_timer();
        }

        when_failed(amxp_timer_new(&update_timer, httpaccess_server_apply_configs_to_server, NULL), exit);
        when_failed(amxp_timer_start(update_timer, httpaccess_get_apply_delay()), exit);
        SAH_TRACEZ_INFO(ME, "Timer Re-armed, new remaining time %d ms", amxp_timer_remaining_time(update_timer));

    } else {
        httpaccess_mod_apply_configs(update_configs);
        httpaccess_server_clean_configs();
    }
    retval = 0;
exit:
    return retval;
}

int httpaccess_server_update_apply_config(amxd_object_t* access) {
    int retval = -1;
    amxc_var_t config;

    when_failed(amxc_var_init(&config), exit);
    when_null(access, exit);

    when_failed(httpaccess_config_update(access, &config), exit);
    when_failed(httpaccess_server_add_config_to_configs(&config), exit);
    when_failed(httpaccess_server_apply_configs(), exit);
    retval = 0;
exit:
    amxc_var_clean(&config);
    return retval;
}

int httpaccess_server_delete_config(amxd_object_t* access) {
    int retval = -1;
    amxc_var_t config;

    when_failed(amxc_var_init(&config), exit);
    when_null(access, exit);

    httpaccess_server_remove_existing_config(HTTPACCESS_GET_ACCESS_ALIAS(access));
    when_failed(httpaccess_config_delete(HTTPACCESS_GET_ACCESS_ALIAS(access), &config), exit);
    httpaccess_mod_delete_config(&config);
    retval = 0;
exit:
    amxc_var_clean(&config);
    return retval;
}
