/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <unistd.h>
#include <fcntl.h>
#include <string.h>
#include <stdlib.h>
#include <amxc/amxc.h>
#include <amxc/amxc_macros.h>
#include <amxp/amxp.h>
#include <amxd/amxd_object.h>
#include <amxd/amxd_transaction.h>
#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>
#include "tr181-httpaccess.h"
#include "httpaccess_session.h"

#define ME "session"

int create_session_id(char* session_id) {
    int rv = -1;
    uint8_t rand_buffer[SESSIONID_LENGTH] = {0};
    const char charset[] = SESSIONID_CHARSET;
    int fd;

    fd = open("/dev/urandom", O_RDONLY);
    when_true(fd == -1, exit);
    when_false(read(fd, rand_buffer, SESSIONID_LENGTH) == SESSIONID_LENGTH, exit);
    for(uint32_t i = 0; i < SESSIONID_LENGTH; ++i) {
        session_id[i] = charset[rand_buffer[i] % (sizeof(charset) - 1)];
    }
    rv = 0;
exit:
    close(fd);
    return rv;
}

void httpaccess_session_deinit(UNUSED const amxc_var_t* data, void* priv) {
    amxd_object_t* session = (amxd_object_t*) priv;
    httpaccess_session_t* session_priv = NULL;

    when_null(session, exit);
    when_null(session->priv, exit);
    session_priv = (httpaccess_session_t*) session->priv;
    amxp_timer_delete(&session_priv->absolute);
    amxp_timer_delete(&session_priv->idle);
    free(session_priv);
    session->priv = NULL;
exit:
    return;
}

static void httpaccess_session_timeout(amxp_timer_t* timer, void* priv) {
    amxd_object_t* session = (amxd_object_t*) priv;
    httpaccess_session_t* session_priv = (httpaccess_session_t*) session->priv;
    const char* status = (session_priv->absolute == timer) ? "AbsoluteTimeout" : "IdleTimeout";
    amxd_trans_t transaction;

    amxd_trans_init(&transaction);
    amxd_trans_set_attr(&transaction, amxd_tattr_change_ro, true);

    SAH_TRACEZ_NOTICE(ME, "Session timeout %s: %s", status, GET_CHAR(amxd_object_get_param_value(session, "ID"), NULL));
    amxd_trans_select_object(&transaction, session);
    amxd_trans_set_value(cstring_t, &transaction, "Status", status);
    amxp_timer_stop(session_priv->absolute);
    amxp_timer_stop(session_priv->idle);
    amxp_sigmngr_deferred_call(NULL, httpaccess_session_deinit, NULL, session);

    amxd_trans_apply(&transaction, httpaccess_get_dm());
    amxd_trans_clean(&transaction);
}

void httpaccess_session_alive(amxd_object_t* session) {
    httpaccess_session_t* priv = NULL;

    when_null(session, exit);
    when_null(session->priv, exit);
    priv = (httpaccess_session_t*) session->priv;
    amxp_timer_start(priv->idle, 1000 * GET_UINT32(amxd_object_get_param_value(session, "IdleTimeout"), NULL));
exit:
    return;
}

void httpaccess_session_added(amxd_object_t* session) {
    amxc_ts_t start_date = {0};
    httpaccess_session_t* priv = NULL;

    amxc_ts_now(&start_date);
    amxd_object_set_amxc_ts_t(session, "StartDate", &start_date);
    when_not_null(session->priv, exit);
    priv = calloc(1, sizeof(*priv));
    when_null(priv, exit);
    session->priv = priv;
    amxp_timer_new(&priv->absolute, httpaccess_session_timeout, session);
    amxp_timer_new(&priv->idle, httpaccess_session_timeout, session);
    amxp_timer_start(priv->absolute, 1000 * GET_UINT32(amxd_object_get_param_value(session, "AbsoluteTimeout"), NULL));
    amxp_timer_start(priv->idle, 1000 * GET_UINT32(amxd_object_get_param_value(session, "IdleTimeout"), NULL));
exit:
    return;
}