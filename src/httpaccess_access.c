/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdlib.h>
#include <string.h>
#include <amxc/amxc.h>
#include <amxc/amxc_macros.h>
#include <amxp/amxp.h>
#include <amxd/amxd_transaction.h>
#include <netmodel/common_api.h>
#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>
#include "tr181-httpaccess.h"
#include "dm_tr181-httpaccess.h"
#include "httpaccess_access.h"
#include "httpaccess_config.h"
#include "httpaccess_server.h"
#include "httpaccess_firewall.h"

#define ME "access"

#define STRING_EMPTY(TEXT) ((TEXT == NULL) || (*TEXT == 0))

void httpaccess_access_cleanup(void) {
    amxd_object_t* accesses = amxd_dm_findf(httpaccess_get_dm(), "%s", HTTPACCESS_TR181_DEVICE_USERINTERFACE_HTTPACCESS_PATH);

    amxd_object_for_each(instance, access_it, accesses) {
        httpaccess_access_deinit(amxc_container_of(access_it, amxd_object_t, it));
    }
}

void httpaccess_access_set_status(const char* alias, const char* status) {
    amxd_object_t* access = NULL;
    amxd_trans_t trans;

    amxd_trans_init(&trans);

    when_str_empty(alias, exit);
    when_str_empty(status, exit);

    access = amxd_dm_findf(httpaccess_get_dm(), "%s%s", HTTPACCESS_TR181_DEVICE_USERINTERFACE_HTTPACCESS_OBJECT, alias);
    when_null(access, exit);

    amxd_trans_select_object(&trans, access);
    amxd_trans_set_attr(&trans, amxd_tattr_change_ro, true);
    SAH_TRACEZ_INFO(ME, "Access %s changed its status: %s -> %s", alias, HTTPACCESS_GET_ACCESS_PARAM(cstring_t, access, "Status"), status);
    amxd_trans_set_cstring_t(&trans, "Status", status);
    amxd_trans_apply(&trans, httpaccess_get_dm());
exit:
    amxd_trans_clean(&trans);
    return;
}

amxd_object_t* httpaccess_access_get_by_alias(const char* alias) {
    amxd_object_t* access = NULL;

    when_str_empty(alias, exit);
    access = amxd_dm_findf(httpaccess_get_dm(), "%s%s", HTTPACCESS_TR181_DEVICE_USERINTERFACE_HTTPACCESS_OBJECT, alias);
exit:
    return access;
}

void httpaccess_access_finalize(const char* alias, bool success) {
    if(success) {
        success = (httpaccess_firewall_add_access(alias) == 0);
    }
    httpaccess_access_set_status(alias, (success ? "Up" : "Error"));
}

static void log_access_address_list(UNUSED amxd_object_t* access, const amxc_var_t* address_list) {

    if(TRACE_LEVEL_INFO <= sahTraceZoneLevel(sahTraceGetZone(ME))) {
        SAH_TRACEZ_INFO(ME, "### Address has changed: for %s / %s",
                        HTTPACCESS_GET_ACCESS_ALIAS(access), HTTPACCESS_GET_ACCESS_INTERFACE(access));
        amxc_var_for_each(address, address_list) {
            SAH_TRACEZ_INFO(ME, "      %s [%s/%s/%s]", GET_CHAR(address, "Address"),
                            GET_CHAR(address, "NetDevName"),
                            GET_CHAR(address, "TypeFlags"),
                            GET_CHAR(address, "Flags"));
        }
    }
}

void httpaccess_access_address_changed(UNUSED const char* sig_name, const amxc_var_t* data, void* priv) {
    amxd_object_t* access = (amxd_object_t*) priv;
    httpaccess_state_t* state = NULL;
    amxc_var_t* ipv6_lla = NULL;
    amxc_var_t* ipv6_gua = NULL;
    bool is_remote = false;

    when_null(access, exit);
    when_null(access->priv, exit);
    log_access_address_list(access, data);

    if(strcmp(HTTPACCESS_GET_ACCESS_PARAM(cstring_t, access, "AccessType"), "RemoteAccess") == 0) {
        is_remote = true;
    }

    state = (httpaccess_state_t*) access->priv;
    amxc_var_delete(&state->ipv4_info);
    amxc_var_delete(&state->ipv6_info_list);
    amxc_var_for_each_reverse(ip, data) {
        if(strcmp(GETP_CHAR(ip, "Family"), "ipv4") == 0) {
            amxc_var_delete(&state->ipv4_info);
            amxc_var_new(&state->ipv4_info);
            amxc_var_copy(state->ipv4_info, ip);
        } else if(strcmp(GETP_CHAR(ip, "Family"), "ipv6") == 0) {
            const cstring_t type = GETP_CHAR(ip, "TypeFlags");
            if(!STRING_EMPTY(type) && (strstr(type, "@gua") != NULL)) {
                amxc_var_delete(&ipv6_gua);
                amxc_var_new(&ipv6_gua);
                amxc_var_copy(ipv6_gua, ip);
            } else if((!STRING_EMPTY(type) && (strstr(type, "@lla") != NULL) && (!is_remote))) {
                amxc_var_delete(&ipv6_lla);
                amxc_var_new(&ipv6_lla);
                amxc_var_copy(ipv6_lla, ip);
            }
        }
    }

    if((ipv6_gua != NULL) || (ipv6_lla != NULL)) {
        amxc_var_new(&state->ipv6_info_list);
        amxc_var_set_type(state->ipv6_info_list, AMXC_VAR_ID_LIST);

        if(ipv6_gua != NULL) {
            amxc_var_t* gua_address = amxc_var_add_new(state->ipv6_info_list);
            amxc_var_move(gua_address, ipv6_gua);
        }

        if(ipv6_lla != NULL) {
            amxc_var_t* lla_address = amxc_var_add_new(state->ipv6_info_list);
            amxc_var_move(lla_address, ipv6_lla);
        }
    }

    if((state->ipv4_info != NULL) || (state->ipv6_info_list != NULL)) {
        SAH_TRACEZ_INFO(ME, "Apply changes");
        httpaccess_server_update_apply_config(access);
    }

exit:

    amxc_var_delete(&ipv6_lla);
    amxc_var_delete(&ipv6_gua);

    return;
}

int httpaccess_access_init_netmodel(amxd_object_t* access) {
    int retval = -1;
    httpaccess_state_t* state = NULL;

    when_null(access, exit);
    when_null(access->priv, exit);
    state = (httpaccess_state_t*) access->priv;
    if(state->query_ips != NULL) {
        netmodel_closeQuery(state->query_ips);
    }
    amxc_var_delete(&state->ipv4_info);
    amxc_var_delete(&state->ipv6_info_list);

    state->query_ips = netmodel_openQuery_getAddrs(HTTPACCESS_GET_ACCESS_PARAM(cstring_t, access, "Interface"), "tr181-httpaccess",
                                                   "ipv4 || (ipv6  && !tentative)", "down",
                                                   httpaccess_access_address_changed, access);
    when_null_trace(state->query_ips, exit, ERROR, "Couldn't open netmodel query for addrs (%s)", HTTPACCESS_GET_ACCESS_ALIAS(access));
    retval = 0;
exit:
    return retval;
}

int httpaccess_access_init(amxd_object_t* access) {
    int retval = -1;
    httpaccess_state_t* state = NULL;

    when_null(access, exit);
    when_not_null(access->priv, exit);
    state = calloc(1, sizeof(*state));
    when_null(state, exit);
    access->priv = state;
    retval = 0;
exit:
    return retval;
}

void httpaccess_access_deinit(amxd_object_t* access) {
    httpaccess_state_t* state = NULL;

    when_null(access, exit);
    httpaccess_firewall_del_access(HTTPACCESS_GET_ACCESS_ALIAS(access));
    state = (httpaccess_state_t*) access->priv;
    if(state != NULL) {
        if(state->query_ips != NULL) {
            netmodel_closeQuery(state->query_ips);
        }
        amxc_var_delete(&state->ipv4_info);
        amxc_var_delete(&state->ipv6_info_list);
        free(state);
        access->priv = NULL;
    }
exit:
    return;
}

int httpaccess_access_added(amxd_object_t* access) {
    int retval = -1;

    when_null(access, exit);
    when_failed_trace(httpaccess_access_init(access), exit, ERROR, "Error while initializing access %s", HTTPACCESS_GET_ACCESS_ALIAS(access));
    amxd_object_set_cstring_t(access, "Status", "Down");
    if(HTTPACCESS_IS_ACCESS_ENABLED(access) == true) {
        when_failed(httpaccess_access_init_netmodel(access), exit);
        when_failed(httpaccess_server_update_apply_config(access), exit);
    }
    retval = 0;
exit:
    return retval;
}

int httpaccess_access_ip_fitering_changed(amxd_object_t* access) {
    int retval = -1;

    when_null(access, exit);
    httpaccess_firewall_del_access(HTTPACCESS_GET_ACCESS_ALIAS(access));
    when_failed(httpaccess_firewall_add_access(HTTPACCESS_GET_ACCESS_ALIAS(access)), exit);
    retval = 0;
exit:
    return retval;
}

int httpaccess_access_changed(amxd_object_t* access, const amxc_var_t* changes) {
    int retval = -1;

    when_null(access, exit);
    if((GETP_ARG(changes, "parameters.Interface.to") != NULL) || (GETP_ARG(changes, "parameters.IPVersion.to") != NULL)) {
        when_failed(httpaccess_access_init_netmodel(access), exit);
    }
    if((GETP_ARG(changes, "parameters.AccessType.to") != NULL) || (GETP_ARG(changes, "parameters.IPVersion.to") != NULL)) {
        httpaccess_firewall_del_access(HTTPACCESS_GET_ACCESS_ALIAS(access));
        httpaccess_firewall_add_access(HTTPACCESS_GET_ACCESS_ALIAS(access));
    }
    if(GETP_ARG(changes, "parameters.Enable.to") != NULL) {
        if(GETP_BOOL(changes, "parameters.Enable.to") == true) {
            when_failed(httpaccess_access_init_netmodel(access), exit);
            when_failed(httpaccess_server_update_apply_config(access), exit);
        } else {
            when_failed(httpaccess_access_disabled(access), exit);
        }
    }
    if(HTTPACCESS_IS_ACCESS_ENABLED(access) == true) {
        when_failed(httpaccess_server_update_apply_config(access), exit);
    }
    retval = 0;
exit:
    if((access != NULL) && (retval != 0)) {
        httpaccess_server_delete_config(access);
        httpaccess_access_set_status(HTTPACCESS_GET_ACCESS_ALIAS(access), "Error");
    }
    return retval;
}

int httpaccess_access_disabled(amxd_object_t* access) {
    int retval = -1;
    httpaccess_state_t* state = NULL;

    when_null(access, exit);
    httpaccess_firewall_del_access(HTTPACCESS_GET_ACCESS_ALIAS(access));
    when_failed(httpaccess_server_delete_config(access), exit);
    state = (httpaccess_state_t*) access->priv;
    if((state != NULL) && (state->query_ips != NULL)) {
        netmodel_closeQuery(state->query_ips);
        state->query_ips = NULL;
    }
    retval = 0;
exit:
    return retval;
}
