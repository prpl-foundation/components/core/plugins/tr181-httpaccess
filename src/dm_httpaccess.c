/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <string.h>
#include <amxc/amxc.h>
#include <amxc/amxc_macros.h>
#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>
#include <amxp/amxp.h>
#include <amxd/amxd_transaction.h>
#include "tr181-httpaccess.h"
#include "dm_tr181-httpaccess.h"
#include "httpaccess_access.h"
#include "httpaccess_session.h"

#define ME "dm"
#define MAX_INACTIVE_SESSION 5

void _dm_httpaccess_changed(UNUSED const char* const sig_name,
                            const amxc_var_t* const data,
                            UNUSED void* const priv) {
    amxd_object_t* access = amxd_dm_signal_get_object(httpaccess_get_dm(), data);

    when_null(access, exit);
    SAH_TRACEZ_NOTICE(ME, "HTTPAccess %s has been changed", HTTPACCESS_GET_ACCESS_ALIAS(access));
    httpaccess_access_changed(access, data);
exit:
    return;
}

void _dm_httpaccess_ip_filtering_changed(UNUSED const char* const sig_name,
                                         const amxc_var_t* const data,
                                         UNUSED void* const priv) {
    int retval = -1;
    amxd_object_t* access = amxd_dm_signal_get_object(httpaccess_get_dm(), data);

    when_null(access, exit);
    SAH_TRACEZ_NOTICE(ME, "HTTPAccess %s ip filtering has been changed", HTTPACCESS_GET_ACCESS_ALIAS(access));
    retval = httpaccess_access_ip_fitering_changed(access);
    httpaccess_access_set_status(HTTPACCESS_GET_ACCESS_ALIAS(access), (retval != 0) ? "Error" : "Up");
exit:
    return;
}

void _dm_httpaccess_added(UNUSED const char* const sig_name,
                          const amxc_var_t* const data,
                          UNUSED void* const priv) {
    amxd_object_t* accesses = amxd_dm_signal_get_object(httpaccess_get_dm(), data);
    amxd_object_t* access = amxd_object_get_instance(accesses, NULL, GET_UINT32(data, "index"));

    when_null(access, exit);
    SAH_TRACEZ_NOTICE(ME, "HTTPAccess %s has been added", HTTPACCESS_GET_ACCESS_ALIAS(access));
    httpaccess_access_added(access);
exit:
    return;
}

amxd_status_t _dm_httpaccess_destroyed(amxd_object_t* object,
                                       UNUSED amxd_param_t* param,
                                       amxd_action_t reason,
                                       UNUSED const amxc_var_t* const args,
                                       amxc_var_t* const retval,
                                       UNUSED void* priv) {
    amxd_status_t status = amxd_status_ok;

    if(reason != action_object_destroy) {
        status = amxd_status_function_not_implemented;
        goto leave;
    }

    SAH_TRACEZ_NOTICE(ME, "HTTPAccess %s has been removed", HTTPACCESS_GET_ACCESS_ALIAS(object));

    if(object->priv != NULL) {
        httpaccess_access_deinit(object);
    }

    amxc_var_clean(retval);
leave:
    return status;
}

static int build_role_set(amxb_bus_ctx_t* users_bus, amxc_set_t* set, const char* roles) {
    int rv = -1;
    amxc_llist_t parts;
    amxc_string_t str;
    amxc_var_t role;

    amxc_string_init(&str, 0);
    amxc_llist_init(&parts);
    amxc_var_init(&role);

    when_null(set, exit);
    when_null(roles, exit);
    when_null(users_bus, exit);

    amxc_string_set(&str, roles);
    when_failed(amxc_string_split_to_llist(&str, &parts, ','), exit);
    amxc_llist_for_each(it, &parts) {
        amxc_string_t* part = amxc_container_of(it, amxc_string_t, it);
        if((amxc_string_text_length(part) > 0) && (*amxc_string_get(part, amxc_string_text_length(part) - 1) != '.')) {
            amxc_string_append(part, ".", 1);
        }
        when_failed(amxb_get(users_bus, amxc_string_get(part, 0), 0, &role, 1), exit);
        when_str_empty(GETP_CHAR(&role, "0.0.RoleName"), exit);
        amxc_set_add_flag(set, GETP_CHAR(&role, "0.0.RoleName"));
    }
    rv = 0;
exit:
    amxc_llist_clean(&parts, amxc_string_list_it_free);
    amxc_string_clean(&str);
    amxc_var_clean(&role);
    return rv;
}

static int set_websession_roles_from_user(amxd_trans_t* trans, amxd_object_t* access, const char* user_path) {
    int rv = -1;
    amxc_var_t ret;
    amxc_set_t access_set;
    amxc_set_t user_set;
    char* roles = NULL;
    amxb_bus_ctx_t* users_bus = NULL;
    amxc_var_init(&ret);
    amxc_set_init(&access_set, false);
    amxc_set_init(&user_set, false);

    when_null(trans, exit);
    when_null(access, exit);
    when_str_empty(user_path, exit);

    users_bus = amxb_be_who_has("Users");
    when_null_trace(users_bus, exit, ERROR, "Failed to get bus ctx of Users");

    when_failed(amxb_get(users_bus, user_path, 0, &ret, 1), exit);
    when_str_empty(GETP_CHAR(&ret, "0.0.RoleParticipation"), exit);

    when_failed(build_role_set(users_bus, &access_set, GET_CHAR(amxd_object_get_param_value(access, "AllowedRoles"), NULL)), exit);
    when_failed(build_role_set(users_bus, &user_set, GETP_CHAR(&ret, "0.0.RoleParticipation")), exit);
    amxc_set_intersect(&access_set, &user_set);
    roles = amxc_set_to_string_sep(&access_set, ",");
    amxd_trans_set_cstring_t(trans, "Roles", roles);
    rv = 0;
exit:
    amxc_var_clean(&ret);
    amxc_set_clean(&access_set);
    amxc_set_clean(&user_set);
    free(roles);
    return rv;
}

amxd_status_t _CreateWebSession(amxd_object_t* object,
                                UNUSED amxd_function_t* func,
                                amxc_var_t* args,
                                amxc_var_t* ret) {
    amxd_status_t status = amxd_status_ok;
    char session_id[SESSIONID_LENGTH + 1] = {0};
    amxd_trans_t* trans = NULL;

    amxd_trans_new(&trans);
    amxd_trans_set_attr(trans, amxd_tattr_change_ro, true);
    amxd_trans_select_object(trans, amxd_object_findf(object, "Session."));
    amxd_trans_add_inst(trans, 0, NULL);
    amxd_trans_set_cstring_t(trans, "User", GET_CHAR(args, "user"));
    amxd_trans_set_cstring_t(trans, "Status", "Active");
    amxd_trans_set_cstring_t(trans, "IPAddress", GET_CHAR(args, "ip"));
    amxd_trans_set_uint32_t(trans, "Port", GET_UINT32(args, "port"));
    amxd_trans_set_cstring_t(trans, "Protocol", GET_CHAR(args, "protocol"));
    amxd_trans_set_uint32_t(trans, "AbsoluteTimeout", GET_UINT32(args, "absolute_timeout"));
    amxd_trans_set_uint32_t(trans, "IdleTimeout", GET_UINT32(args, "idle_timeout"));
    when_failed(set_websession_roles_from_user(trans, object, GET_CHAR(args, "user")), exit);
    do {
        when_failed(create_session_id(session_id), exit);
        amxd_trans_set_cstring_t(trans, "ID", session_id);
        status = amxd_trans_apply(trans, httpaccess_get_dm());
    } while(status == amxd_status_duplicate);
    amxc_var_set_type(ret, AMXC_VAR_ID_HTABLE);
    if(status == amxd_status_ok) {
        amxc_var_add_key(cstring_t, ret, "session_id", session_id);
    } else {
        SAH_TRACEZ_ERROR(ME, "Session creation failed with status: %d", status);
    }
exit:
    amxd_trans_delete(&trans);
    return status;
}

static bool check_session_id_in_charset(const char* id) {
    bool ret = false;

    when_str_empty(id, exit);
    for(uint32_t i = 0; id[i]; ++i) {
        when_null(strchr(SESSIONID_CHARSET, id[i]), exit);
    }
    ret = true;
exit:
    return ret;
}

amxd_status_t _CheckSessionValid(amxd_object_t* object,
                                 UNUSED amxd_function_t* func,
                                 amxc_var_t* args,
                                 amxc_var_t* ret) {
    amxd_status_t status = amxd_status_ok;
    amxd_object_t* session = NULL;
    const char* id = NULL;
    const char* ip = NULL;

    amxc_var_set(bool, ret, false);
    id = GET_CHAR(args, "id");
    ip = GET_CHAR(args, "ip");
    when_str_empty(ip, exit);
    when_false(check_session_id_in_charset(id), exit);
    session = amxd_object_findf(object, "Session.[ID == \"%s\"].", id);
    when_null_trace(session, exit, WARNING, "Session %s hasn't been found for this access: %s", id, amxd_object_get_path(object, AMXD_OBJECT_TERMINATE));
    when_false_trace(strcmp(ip, GET_CHAR(amxd_object_get_param_value(session, "IPAddress"), NULL)) == 0, exit, WARNING, "IP Address:%s try to access session %s that it doesn't own", ip, id);
    when_false_trace(strcmp("Active", GET_CHAR(amxd_object_get_param_value(session, "Status"), NULL)) == 0, exit, WARNING, "IP Address: %s try to access session %s which isn't active", ip, id);
    httpaccess_session_alive(session);
    amxc_var_set(bool, ret, true);
exit:
    return status;
}

void _dm_httpaccess_session_added(UNUSED const char* const sig_name,
                                  const amxc_var_t* const data,
                                  UNUSED void* const priv) {
    amxd_object_t* sessions = amxd_dm_signal_get_object(httpaccess_get_dm(), data);
    amxd_object_t* session = amxd_object_get_instance(sessions, GET_CHAR(data, "name"), 0);

    when_null(session, exit);
    SAH_TRACEZ_NOTICE(ME, "HTTPAccess session %d has been added", amxd_object_get_index(session));
    httpaccess_session_added(session);
exit:
    return;
}

void _dm_httpaccess_session_changed(UNUSED const char* const sig_name,
                                    const amxc_var_t* const data,
                                    UNUSED void* const priv) {
    amxd_object_t* session = amxd_dm_signal_get_object(httpaccess_get_dm(), data);
    amxd_object_t* sessions = amxd_object_get_parent(session);
    amxc_llist_t inactive_sessions;
    int max_inactive_session;
    int inactive_count = 0;
    amxd_trans_t transaction;

    amxc_llist_init(&inactive_sessions);
    amxd_trans_init(&transaction);
    amxd_trans_set_attr(&transaction, amxd_tattr_change_ro, true);

    SAH_TRACEZ_NOTICE(ME, "HTTPAccess session %d status changed", amxd_object_get_index(session));

    max_inactive_session = (GET_ARG(httpaccess_get_config(), "max-inactive-session") != NULL) ? GET_UINT32(httpaccess_get_config(), "max-inactive-session") : MAX_INACTIVE_SESSION;

    amxd_object_resolve_pathf(sessions, &inactive_sessions, "[Status != 'Active']");
    inactive_count = amxc_llist_size(&inactive_sessions);
    when_true(inactive_count <= max_inactive_session, exit);

    amxd_trans_select_object(&transaction, sessions);
    amxc_llist_for_each(it, &inactive_sessions) {
        amxc_string_t* path = amxc_string_from_llist_it(it);
        amxd_object_t* inactive = amxd_dm_findf(httpaccess_get_dm(), "%s", amxc_string_get(path, 0));
        SAH_TRACEZ_NOTICE(ME, "HTTPAccess delete inactive session %d", amxd_object_get_index(inactive));
        amxd_trans_del_inst(&transaction, amxd_object_get_index(inactive), NULL);
        inactive_count--;
        if(inactive_count <= max_inactive_session) {
            break;
        }
    }

    amxd_trans_apply(&transaction, httpaccess_get_dm());

exit:
    amxd_trans_clean(&transaction);
    amxc_llist_clean(&inactive_sessions, amxc_string_list_it_free);
}

amxd_status_t _dm_httpaccess_session_destroyed(amxd_object_t* object,
                                               UNUSED amxd_param_t* param,
                                               amxd_action_t reason,
                                               UNUSED const amxc_var_t* const args,
                                               amxc_var_t* const retval,
                                               UNUSED void* priv) {
    amxd_status_t status = amxd_status_ok;

    if(reason != action_object_destroy) {
        status = amxd_status_function_not_implemented;
        goto leave;
    }

    SAH_TRACEZ_NOTICE(ME, "HTTPAccess session %s has been removed", amxd_object_get_name(object, AMXD_OBJECT_INDEXED));

    if(object->priv != NULL) {
        httpaccess_session_deinit(NULL, object);
    }

    amxc_var_clean(retval);
leave:
    return status;
}

amxd_status_t _Delete(amxd_object_t* object,
                      UNUSED amxd_function_t* func,
                      UNUSED amxc_var_t* args,
                      UNUSED amxc_var_t* ret) {
    amxd_status_t status = amxd_status_ok;
    amxd_trans_t transaction;

    amxd_trans_init(&transaction);
    amxd_trans_set_attr(&transaction, amxd_tattr_change_ro, true);
    SAH_TRACEZ_NOTICE(ME, "Session delete: %s", GET_CHAR(amxd_object_get_param_value(object, "ID"), NULL));
    amxd_trans_select_object(&transaction, object);
    amxd_trans_set_value(cstring_t, &transaction, "Status", "Closed");
    status = amxd_trans_apply(&transaction, httpaccess_get_dm());
    amxd_trans_clean(&transaction);

    return status;
}