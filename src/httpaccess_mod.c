/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <amxc/amxc.h>
#include <amxc/amxc_macros.h>
#include <amxm/amxm.h>
#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>
#include "tr181-httpaccess.h"
#include "httpaccess_mod.h"
#include "httpaccess_access.h"

#define ME "mod"

static const char* httpaccess_mod_get_controller(void) {
    const char* controller = "";
    amxd_object_t* ui = amxd_dm_findf(httpaccess_get_dm(), HTTPACCESS_TR181_DEVICE_USERINTERFACE_PATH);

    when_null(ui, exit);
    controller = amxc_var_constcast(cstring_t, amxd_object_get_param_value(ui, "Controller"));
exit:
    return controller;
}

int httpaccess_mod_init(void) {
    int retval = 0;
    amxd_dm_t* dm = httpaccess_get_dm();
    amxd_object_t* instance = amxd_dm_findf(dm, HTTPACCESS_TR181_DEVICE_USERINTERFACE_PATH);
    const amxc_var_t* controllers = amxd_object_get_param_value(instance, "HTTPAccessSupportedControllers");

    amxc_var_t lcontrollers;
    amxm_shared_object_t* so = NULL;
    amxc_string_t mod_path;

    amxc_var_init(&lcontrollers);
    amxc_string_init(&mod_path, 0);

    amxc_var_convert(&lcontrollers, controllers, AMXC_VAR_ID_LIST);
    amxc_var_for_each(controller, &lcontrollers) {
        const char* name = GET_CHAR(controller, NULL);
        amxc_var_t ret;

        amxc_var_init(&ret);
        amxc_string_setf(&mod_path, "/usr/lib/amx/tr181-httpaccess/modules/%s.so", name);
        SAH_TRACEZ_INFO(ME, "Open HTTPAccess mod %s", name);
        retval = amxm_so_open(&so, name, amxc_string_get(&mod_path, 0));
        when_failed_trace(retval, clean, ERROR, "Error while opening mod %s", amxc_string_get(&mod_path, 0));
        if(amxm_execute_function(httpaccess_mod_get_controller(), MOD_HTTPACCESS_CTRL, "init", httpaccess_get_config(), &ret) != 0) {
            retval = -1;
            SAH_TRACEZ_ERROR(ME, "Error during initialization of mod %s", name);
        }
clean:
        amxc_var_clean(&ret);
        if(retval == -1) {
            break;
        }
    }
    amxc_var_clean(&lcontrollers);
    amxc_string_clean(&mod_path);
    return retval;
}

void httpaccess_mod_apply_configs(amxc_var_t* configs) {
    amxc_var_t ret;

    SAH_TRACEZ_IN(ME);
    amxc_var_init(&ret);
    when_failed_trace(amxm_execute_function(httpaccess_mod_get_controller(), MOD_HTTPACCESS_CTRL, "update-accesses", configs, &ret), exit, ERROR, "Unable to apply config to module");

    amxc_htable_for_each(config, amxc_var_constcast(amxc_htable_t, &ret)) {
        bool success = amxc_var_constcast(bool, amxc_htable_it_get_data(config, amxc_var_t, hit));
        if(success == false) {
            SAH_TRACEZ_ERROR(ME, "Couldn't apply config %s", config->key);
        }
        httpaccess_access_finalize(config->key, success);
    }
exit:
    amxc_var_clean(&ret);
    SAH_TRACEZ_OUT(ME);
    return;
}

void httpaccess_mod_delete_config(amxc_var_t* config) {
    amxc_var_t ret;
    amxd_object_t* accesses = amxd_dm_findf(httpaccess_get_dm(), "%s", HTTPACCESS_TR181_DEVICE_USERINTERFACE_HTTPACCESS_PATH);

    SAH_TRACEZ_IN(ME);
    when_failed(amxc_var_init(&ret), exit);

    when_failed(amxm_execute_function(httpaccess_mod_get_controller(), MOD_HTTPACCESS_CTRL, "delete-access", config, &ret), exit);

    amxd_object_for_each(instance, access_it, accesses) {
        amxd_object_t* access = amxc_container_of(access_it, amxd_object_t, it);
        if(GET_ARG(&ret, HTTPACCESS_GET_ACCESS_ALIAS(access))) {
            if(GET_BOOL(&ret, HTTPACCESS_GET_ACCESS_ALIAS(access)) == false) {
                SAH_TRACEZ_ERROR(ME, "Couldn't apply config %s", HTTPACCESS_GET_ACCESS_ALIAS(access));
                httpaccess_access_set_status(HTTPACCESS_GET_ACCESS_ALIAS(access), "Error");
            } else {
                httpaccess_access_set_status(HTTPACCESS_GET_ACCESS_ALIAS(access), "Up");
            }
        } else {
            httpaccess_access_set_status(HTTPACCESS_GET_ACCESS_ALIAS(access), "Down");
        }
    }
exit:
    amxc_var_clean(&ret);
    SAH_TRACEZ_OUT(ME);
    return;
}

void httpaccess_mod_start(void) {
    amxc_var_t ret;
    amxc_var_t data;

    SAH_TRACEZ_IN(ME);
    amxc_var_init(&data);
    amxc_var_init(&ret);

    when_failed_trace(amxm_execute_function(httpaccess_mod_get_controller(), MOD_HTTPACCESS_CTRL, "start", &data, &ret), exit, ERROR, "Unable to start back-end");

exit:
    amxc_var_clean(&ret);
    amxc_var_clean(&data);
    SAH_TRACEZ_OUT(ME);
    return;
}