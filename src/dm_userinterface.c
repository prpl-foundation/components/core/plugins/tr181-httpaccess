/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <amxc/amxc.h>
#include <amxc/amxc_macros.h>
#include <amxp/amxp.h>
#include <amxd/amxd_types.h>
#include <amxb/amxb.h>
#include <amxd/amxd_transaction.h>
#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>
#include <debug/user_trace.h>
#include <string.h>
#include "tr181-httpaccess.h"
#include "dm_userinterface.h"
#include "httpaccess_access.h"

#define ME "dm_userintf"

static const char* ui_get_prefix(void) {
    amxc_var_t* setting = amxo_parser_get_config(httpaccess_get_parser(), "prefix_");
    const char* prefix = GET_CHAR(setting, NULL);
    return (prefix != NULL) ? prefix : "";
}

static int get_lcs_factory_password(amxb_bus_ctx_t* bus, amxc_var_t* factory_password) {
    int rv = -1;
    amxc_var_t response;
    amxc_var_t* value = NULL;

    amxc_var_init(&response);

    when_null_trace(bus, exit, ERROR, "Bus ctx is NULL");
    when_null_trace(factory_password, exit, ERROR, "factory_password variable is NULL");

    when_failed_trace(amxb_get(bus, "LANConfigSecurity.FactoryConfigPassword", 0, &response, 5), exit,
                      ERROR, "Get request to LANConfigSecurity.FactoryConfigPassword failed");
    value = GETP_ARG(&response, "0.0.FactoryConfigPassword");
    when_null_trace(value, exit, ERROR, "Failed to parse get LANConfigSecurity.FactoryConfigPassword response");
    when_failed(amxc_var_move(factory_password, value), exit);

    rv = 0;
exit:
    amxc_var_clean(&response);
    return rv;
}

static int get_lcs_config_password(amxb_bus_ctx_t* bus, amxc_var_t* config_password) {
    int rv = -1;
    amxc_var_t response;
    amxc_var_t* value = NULL;

    amxc_var_init(&response);

    when_null_trace(bus, exit, ERROR, "Bus ctx is NULL");
    when_null_trace(config_password, exit, ERROR, "config_password variable is NULL");

    when_failed_trace(amxb_get(bus, "LANConfigSecurity.ConfigPassword", 0, &response, 5), exit,
                      ERROR, "Get request to LANConfigSecurity.ConfigPassword failed");
    value = GETP_ARG(&response, "0.0.ConfigPassword");
    when_null_trace(value, exit, ERROR, "Failed to parse get LANConfigSecurity.ConfigPassword response");
    when_failed(amxc_var_move(config_password, value), exit);

    rv = 0;
exit:
    amxc_var_clean(&response);
    return rv;
}

static int set_lcs_config_password(amxb_bus_ctx_t* bus, amxc_var_t* password) {
    int rv = -1;
    amxc_var_t response;
    amxc_var_t change;

    amxc_var_init(&response);
    amxc_var_init(&change);
    amxc_var_set_type(&change, AMXC_VAR_ID_HTABLE);

    when_null_trace(bus, exit, ERROR, "Bus ctx is NULL");
    when_null_trace(password, exit, ERROR, "password variable is NULL");

    when_failed_trace(amxc_var_set_key(&change, "ConfigPassword", password, AMXC_VAR_FLAG_COPY), exit,
                      ERROR, "Failed to populate LANConfigSecurity.ConfigPassword change");
    when_failed_trace(amxb_set(bus, "LANConfigSecurity", &change, &response, 5), exit,
                      ERROR, "Request to set LANConfigSecurity.ConfigPassword failed");

    rv = 0;
exit:
    amxc_var_clean(&change);
    amxc_var_clean(&response);
    return rv;
}

amxd_status_t _PasswordReset(UNUSED amxd_object_t* object,
                             UNUSED amxd_function_t* func,
                             UNUSED amxc_var_t* args,
                             UNUSED amxc_var_t* ret) {
    SAH_TRACEZ_IN(ME);
    amxd_status_t rv = amxd_status_unknown_error;
    amxb_bus_ctx_t* bus = NULL;
    amxc_var_t factory_password;

    amxc_var_init(&factory_password);

    bus = amxb_be_who_has("LANConfigSecurity");
    when_null_trace(bus, exit, ERROR, "Failed to get bus ctx of LANConfigSecurity");

    when_failed_trace(get_lcs_factory_password(bus, &factory_password), exit, ERROR, "Failed to get LANConfigSecurity.FactoryConfigPassword");
    when_failed_trace(set_lcs_config_password(bus, &factory_password), exit, ERROR, "Failed to set LANConfigSecurity.ConfigPassword");

    rv = amxd_status_ok;
exit:
    amxc_var_clean(&factory_password);
    SAH_TRACEZ_OUT(ME);
    return rv;
}

static bool is_username_clean(const char* username) {
    bool ret = false;
    amxp_expr_t* expr = NULL;
    amxc_string_t value;

    amxc_string_init(&value, 0);
    amxc_string_appendf(&value, "\"%s\" matches \"^([a-zA-Z_][a-zA-Z0-9_-]*\\$?)$\"", username);
    when_false(amxp_expr_new(&expr, amxc_string_get(&value, 0)) == amxp_expr_status_ok, exit);
    ret = amxp_expr_eval(expr, NULL);
exit:
    amxp_expr_delete(&expr);
    amxc_string_clean(&value);
    return ret;
}

static const char* get_userinterface_allowed_roles(const char* ui) {
    const char* allowed_roles = NULL;
    amxd_object_t* access_obj = NULL;

    access_obj = amxd_dm_findf(httpaccess_get_dm(), "%s", ui);
    when_null_trace(access_obj, exit, ERROR, "UserInterface provided is unknown: %s", ui);

    allowed_roles = GET_CHAR(amxd_object_get_param_value(access_obj, "AllowedRoles"), NULL);
exit:
    return allowed_roles;
}

static const char* skip_prefix(const char* path) {
    const char* prefix = "Device.";

    if(strstr(path, prefix) == &path[0]) {
        path = &path[strlen(prefix)];
    }
    return path;
}

// This function doesn't check if user's groups have correct role by design but it could be changed if requested
static bool is_user_allowed(const char* username, const char* allowed_roles) {
    SAH_TRACEZ_IN(ME);
    bool allowed = false;
    amxb_bus_ctx_t* users_bus = NULL;
    amxc_string_t user_expr;
    amxc_string_t csv_roles;
    amxc_var_t roles;
    amxc_var_t user;
    const char* user_role_participation = NULL;

    amxc_string_init(&user_expr, 0);
    amxc_string_init(&csv_roles, 0);
    amxc_var_init(&roles);
    amxc_var_init(&user);

    users_bus = amxb_be_who_has("Users");
    when_null_trace(users_bus, exit, ERROR, "Failed to get bus ctx of Users");

    amxc_string_set(&csv_roles, allowed_roles);
    amxc_string_setf(&user_expr, "Users.User.[Username == \"%s\"].", username);
    when_failed_trace(amxc_string_csv_to_var(&csv_roles, &roles, NULL), exit, ERROR, "Failed to split allowed_roles %s", allowed_roles);

    when_false_trace(amxb_get(users_bus, amxc_string_get(&user_expr, 0), 0, &user, 1) == AMXB_STATUS_OK, exit, WARNING, "Unknown username: %s", username);
    when_null_trace(GETP_ARG(&user, "0.0."), exit, WARNING, "Unknown username: %s", username);
    user_role_participation = GETP_CHAR(&user, "0.0.RoleParticipation");
    when_str_empty_trace(user_role_participation, exit, WARNING, "User %s has no RoleParticipation", username);

    amxc_var_for_each(role_it, &roles) {
        const char* role = skip_prefix(GET_CHAR(role_it, NULL));
        if(strstr(user_role_participation, role) != NULL) {
            allowed = true;
            break;
        }
    }
exit:
    amxc_var_clean(&user);
    amxc_var_clean(&roles);
    amxc_string_clean(&csv_roles);
    amxc_string_clean(&user_expr);
    SAH_TRACEZ_OUT(ME);
    return allowed;
}

static bool password_is_user_selectable(void) {
    SAH_TRACEZ_IN(ME);
    bool rv = true;
    amxd_object_t* ui_obj = amxd_dm_findf(httpaccess_get_dm(), "UserInterface");
    when_null_trace(ui_obj, exit, ERROR, "Failed to get dm UserInterface object");

    rv = amxd_object_get_value(bool, ui_obj, "PasswordUserSelectable", NULL);
exit:
    SAH_TRACEZ_OUT(ME);
    return rv;
}

static bool is_valid_user_password(const char* username, const char* password) {
    SAH_TRACEZ_IN(ME);
    bool rv = false;
    int ret = 0;
    amxb_bus_ctx_t* users_bus = NULL;
    amxc_var_t check_cred_args;
    amxc_var_t check_cred_ret;

    amxc_var_init(&check_cred_args);
    amxc_var_init(&check_cred_ret);

    amxc_var_set_type(&check_cred_args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &check_cred_args, "Username", username);
    amxc_var_add_key(cstring_t, &check_cred_args, "Password", password);

    users_bus = amxb_be_who_has("Users");
    when_null_trace(users_bus, exit, ERROR, "Failed to get bus ctx of Users");

    ret = amxb_call(users_bus, "Users.", "CheckCredentialsDiagnostics", &check_cred_args, &check_cred_ret, 1);
    when_failed_trace(ret, exit, ERROR, "Failed (%i) to call Users.CheckCredentialsDiagnostics(%s, ...)", ret, username);

    when_str_empty_trace(GETP_CHAR(&check_cred_ret, "1.Status"), exit, ERROR, "Invalid response from Users.CheckCredentialsDiagnostics");

    rv = strcmp(GETP_CHAR(&check_cred_ret, "1.Status"), "Credentials_Good") == 0;
exit:
    amxc_var_clean(&check_cred_args);
    amxc_var_clean(&check_cred_ret);
    SAH_TRACEZ_OUT(ME);
    return rv;
}

static bool is_valid_auto_configuration_password(const char* password) {
    SAH_TRACEZ_IN(ME);
    bool rv = false;
    amxc_var_t lcs_config_password;
    amxc_var_init(&lcs_config_password);
    const char* config_password = NULL;
    amxb_bus_ctx_t* bus = amxb_be_who_has("LANConfigSecurity");

    when_null_trace(bus, exit, ERROR, "Failed to get bus ctx of LANConfigSecurity");

    when_failed_trace(get_lcs_config_password(bus, &lcs_config_password), exit, ERROR, "Failed to get LANConfigSecurity.ConfigPassword");
    config_password = GET_CHAR(&lcs_config_password, NULL);
    when_null_trace(config_password, exit, ERROR, "Failed to extract LANConfigSecurity.ConfigPassword string");

    rv = strcmp(config_password, password) == 0;
exit:
    amxc_var_clean(&lcs_config_password);
    SAH_TRACEZ_OUT(ME);
    return rv;
}

static bool get_login_attempts(const char* prefix, amxd_object_t* obj, uint32_t* attempts) {
    amxc_string_t attempts_param;
    amxc_string_init(&attempts_param, 0);
    bool res = false;
    const amxc_var_t* attempts_var = NULL;

    amxc_string_setf(&attempts_param, "%sLoginAttempts", prefix);

    attempts_var = amxd_object_get_param_value(obj, amxc_string_get(&attempts_param, 0));
    when_null_trace(attempts_var, exit, ERROR, "Cannot get param %s", amxc_string_get(&attempts_param, 0));
    *attempts = amxc_var_constcast(uint32_t, attempts_var);

    res = true;
exit:
    amxc_string_clean(&attempts_param);
    return res;
}

static bool set_login_attempts(const char* prefix, amxd_object_t* obj, bool allowed, uint32_t attempts, amxc_var_t* ret) {
    amxc_string_t attempts_param;
    amxc_string_init(&attempts_param, 0);
    amxd_trans_t* trans = NULL;
    bool success = false;

    amxc_string_setf(&attempts_param, "%sLoginAttempts", prefix);

    amxd_trans_new(&trans);
    amxd_trans_set_attr(trans, amxd_tattr_change_ro, true);
    amxd_trans_select_object(trans, obj);

    if(!allowed) {
        attempts++;
    } else {
        amxc_var_add_key(uint32_t, ret, "LoginAttempts", attempts);
        attempts = 0;
    }
    amxd_trans_set_uint32_t(trans, amxc_string_get(&attempts_param, 0), attempts);
    when_false_trace(amxd_trans_apply(trans, httpaccess_get_dm()) == amxd_status_ok,
                     exit, ERROR, "Could not set %s to %u", amxc_string_get(&attempts_param, 0), attempts);

    success = true;
exit:
    amxc_string_clean(&attempts_param);
    amxd_trans_delete(&trans);
    return success;
}

amxd_status_t _CheckCredentialsForAccess(UNUSED amxd_object_t* object,
                                         UNUSED amxd_function_t* func,
                                         amxc_var_t* args,
                                         amxc_var_t* ret) {
    SAH_TRACEZ_IN(ME);
    bool allowed = false;
    const char* username = GET_CHAR(args, "username");
    const char* password = GET_CHAR(args, "password");
    const char* httpaccess = GET_CHAR(args, "httpaccess");
    const char* allowed_roles = NULL;
    const char* prefix = ui_get_prefix();
    uint32_t attempts = 0;
    amxd_object_t* ui_obj = NULL;

    amxc_var_set_type(ret, AMXC_VAR_ID_HTABLE);

    ui_obj = amxd_dm_findf(httpaccess_get_dm(), "%s", httpaccess);
    when_null_trace(ui_obj, exit, ERROR, "Cannot get obj at %s", httpaccess);

    when_false_trace(is_username_clean(username), exit, WARNING, "Username doesn't match allowed pattern");

    allowed_roles = get_userinterface_allowed_roles(httpaccess);
    when_str_empty_trace(allowed_roles, exit, WARNING, "No allowed roles found for: %s", httpaccess);
    when_false_trace(is_user_allowed(username, allowed_roles), exit,
                     WARNING, "Username: %s, trying to connect but isn't allowed for this access %s", username, allowed_roles);

    when_null_trace(password, exit, ERROR, "No password provided");
    if(password_is_user_selectable()) {
        allowed = is_valid_user_password(username, password);
    } else {
        allowed = is_valid_auto_configuration_password(password);
    }

    SAH_TRACEZ_INFO(ME, "User %s is %sallowed access to %s with those credentials", username, allowed ? "" : "not ", httpaccess);

exit:
    if((ui_obj != NULL) && get_login_attempts(prefix, ui_obj, &attempts)) {
        if(set_login_attempts(prefix, ui_obj, allowed, attempts, ret) == false) {
            SAH_TRACEZ_WARNING(ME, "Setting %sLoginAttempts failed", prefix);
        }
    }
    if(allowed) {
        USER_TRACE_WARNING(TRACE_CAT_SYSTEM, "[Authentication][HTTP][SUCCESS] User='%s'", username);
    } else {
        USER_TRACE_WARNING(TRACE_CAT_SYSTEM, "[Authentication][HTTP][FAILED] User='%s'", username);
    }
    amxc_var_add_key(bool, ret, "Allowed", allowed);
    SAH_TRACEZ_OUT(ME);
    return amxd_status_ok;
}
