/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdlib.h>
#include <string.h>
#include <amxc/amxc.h>
#include <amxc/amxc_macros.h>
#include <amxm/amxm.h>
#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>
#include "tr181-httpaccess.h"
#include "httpaccess_access.h"
#include "httpaccess_firewall.h"

#define ME "firewall"

static amxc_var_t* fw_current_rules = NULL;

static void CONSTRUCTOR httpaccess_firewall_init(void) {
    amxc_var_new(&fw_current_rules);
    amxc_var_set_type(fw_current_rules, AMXC_VAR_ID_HTABLE);
}

static void DESTRUCTOR httpaccess_firewall_deinit(void) {
    amxc_var_delete(&fw_current_rules);
}

amxc_var_t* httpaccess_firewall_get_current_rules(void) {
    return fw_current_rules;
}

static bool httpaccess_firewall_is_remote(amxd_object_t* access) {
    bool is_remote = false;
    when_null(access, exit);

    if(strcmp(HTTPACCESS_GET_ACCESS_PARAM(cstring_t, access, "AccessType"), "RemoteAccess") == 0) {
        is_remote = true;
    }
exit:
    return is_remote;
}

int httpaccess_firewall_load_controller(void) {
    int retval = 0;
    const char* controller = NULL;
    amxm_shared_object_t* so = NULL;

    controller = GETP_CHAR(httpaccess_get_config(), "firewall.controller");
    when_str_empty_trace(controller, exit, INFO, "firewall.controller variable hasn't been configured");

    retval = amxm_so_open(&so, "fw", controller);
    if(retval == -1) {
        SAH_TRACEZ_ERROR(ME, "Error while opening mod %s", controller);
    }
exit:
    return retval;
}

static bool httpaccess_firewall_is_access_in_list(const char* alias, amxc_var_t* list) {
    bool retval = false;

    amxc_var_for_each(access, list) {
        if(strcmp(alias, GET_CHAR(access, NULL)) == 0) {
            retval = true;
            break;
        }
    }
    return retval;
}

static int httpaccess_add_fw_service(const char* serviceid,
                                     const char* interface,
                                     uint32_t port,
                                     uint32_t ipversion,
                                     const char* srcprefix,
                                     amxc_var_t* ret) {
    amxc_var_t args;
    int retval = -1;

    when_str_empty(serviceid, exit);
    when_str_empty(interface, exit);

    amxc_var_init(&args);
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);

    amxc_var_add_key(cstring_t, &args, "id", serviceid);
    amxc_var_add_key(cstring_t, &args, "interface", interface);
    amxc_var_add_key(uint32_t, &args, "destination_port", port);
    amxc_var_add_key(cstring_t, &args, "protocol", "6");
    amxc_var_add_key(uint32_t, &args, "ipversion", ipversion);
    amxc_var_add_key(cstring_t, &args, "source_prefix", srcprefix);
    amxc_var_add_key(bool, &args, "enable", true);

    retval = amxm_execute_function("fw", "fw", "set_service", &args, ret);
    if(retval != 0) {
        SAH_TRACEZ_WARNING(ME, "Firewall failed to add service %s on interface %s ?", serviceid, interface);
    } else {
        SAH_TRACEZ_INFO(ME, "Firewall service[%s] added", serviceid);
    }
exit:
    amxc_var_clean(&args);
    return retval;
}

static int httpaccess_firewall_add_ip_access(amxd_object_t* const access, uint8_t ipversion, const amxc_string_t* fw_id, amxc_var_t* ret) {
    int retval = -1;
    bool allow_all_ip = false;
    const char* allowed_srcprefix = NULL;

    when_null(access, exit);
    when_null(fw_id, exit);
    when_null(ret, exit);

    allow_all_ip = HTTPACCESS_GET_ACCESS_PARAM(bool, access, ipversion == 4 ? "AllowAllIPv4" : "AllowAllIPv6");
    allowed_srcprefix = HTTPACCESS_GET_ACCESS_PARAM(cstring_t, access, ipversion == 4 ? "IPv4AllowedSourcePrefix" : "IPv6AllowedSourcePrefix");

    if(!httpaccess_firewall_is_remote(access)) {
        SAH_TRACEZ_INFO(ME, "Firewall rule %s has no ip prefix filtering (local access)", amxc_string_get(fw_id, 0));
        allowed_srcprefix = "";
    } else if(allow_all_ip) {
        SAH_TRACEZ_INFO(ME, "Firewall rule %s has no ip prefix filtering", amxc_string_get(fw_id, 0));
        allowed_srcprefix = "";
    }

    retval = httpaccess_add_fw_service(amxc_string_get(fw_id, 0),
                                       HTTPACCESS_GET_ACCESS_PARAM(cstring_t, access, "Interface"),
                                       HTTPACCESS_GET_ACCESS_PARAM(uint32_t, access, "Port"),
                                       ipversion,
                                       allowed_srcprefix,
                                       ret);
exit:
    return retval;
}

static amxc_string_t* httpaccess_firewall_generate_fw_service_id(const amxd_object_t* access, uint8_t ipversion) {
    amxc_string_t* fw_id = NULL;

    when_null(access, exit);
    when_failed(amxc_string_new(&fw_id, 0), exit);

    amxc_string_setf(fw_id, "tr181-httpaccess-%s-v%hhu-%d",
                     HTTPACCESS_GET_ACCESS_PARAM(cstring_t, access, "Interface"),
                     ipversion,
                     HTTPACCESS_GET_ACCESS_PARAM(uint32_t, access, "Port"));

    amxc_string_replace(fw_id, ".", "-", UINT32_MAX);

exit:
    return fw_id;
}

static void httpaccess_firewall_mod_del_access(const char* id) {
    amxc_var_t args;
    amxc_var_t ret;
    int rv = -1;

    amxc_var_init(&args);
    amxc_var_init(&ret);
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);

    when_false(amxm_has_function("fw", "fw", "delete_service"), clean);

    amxc_var_add_key(cstring_t, &args, "id", id);
    rv = amxm_execute_function("fw", "fw", "delete_service", &args, &ret);
    when_false_trace(rv == 0, clean, ERROR, "Firewall rule %s's deletion has failed", id);
    SAH_TRACEZ_NOTICE(ME, "Firewall rule %s has been successfully deleted", id);

clean:
    amxc_var_clean(&ret);
    amxc_var_clean(&args);
}

void httpaccess_firewall_del_access(const char* alias) {
    when_str_empty(alias, exit);

    amxc_htable_for_each(rule, amxc_var_constcast(amxc_htable_t, fw_current_rules)) {
        amxc_var_t* rule_list = amxc_htable_it_get_data(rule, amxc_var_t, hit);
        amxc_llist_for_each(access_it, amxc_var_constcast(amxc_llist_t, rule_list)) {
            amxc_var_t* access = amxc_container_of(access_it, amxc_var_t, lit);
            if(strcmp(alias, GET_CHAR(access, NULL)) == 0) {
                amxc_llist_it_take(access_it);
                amxc_var_delete(&access);
                if(amxc_llist_size(amxc_var_constcast(amxc_llist_t, rule_list)) == 0) {
                    httpaccess_firewall_mod_del_access(rule->key);
                    amxc_htable_it_take(rule);
                    amxc_var_delete(&rule_list);
                } else {
                    SAH_TRACEZ_INFO(ME, "The firewall rule %s is still being used by other accesses, not deleting it", rule->key);
                }
                break;
            }
        }
    }
exit:
    return;
}

static int httpaccess_firewall_add_access_ip_version(const char* alias, amxd_object_t* access, uint8_t ip_version) {
    int retval = -1;
    amxc_string_t* fw_id = NULL;
    amxc_var_t* fw_current_rule = NULL;
    amxc_var_t ret;
    const char* fw_id_str = NULL;

    amxc_var_init(&ret);
    when_str_empty(alias, exit);
    when_null(access, exit);
    fw_id = httpaccess_firewall_generate_fw_service_id(access, ip_version);
    fw_id_str = amxc_string_get(fw_id, 0);
    when_str_empty_trace(fw_id_str, exit, ERROR, "Firewall IPv%d rule id %s's creation has failed", ip_version, alias);
    fw_current_rule = GET_ARG(fw_current_rules, fw_id_str);
    if(fw_current_rule == NULL) {
        amxc_var_t* access_list = NULL;

        retval = httpaccess_firewall_add_ip_access(access, ip_version, fw_id, &ret);
        when_failed_trace(retval, exit, ERROR, "Firewall rule %s's creation has failed", fw_id_str);
        amxc_var_new(&access_list);
        amxc_var_set_type(access_list, AMXC_VAR_ID_LIST);
        amxc_var_add(cstring_t, access_list, alias);
        amxc_var_add_key(amxc_llist_t, fw_current_rules, amxc_string_get(fw_id, 0), amxc_var_constcast(amxc_llist_t, access_list));
        SAH_TRACEZ_NOTICE(ME, "Firewall rule %s has been successfully created", fw_id_str);
        amxc_var_delete(&access_list);
    } else if(httpaccess_firewall_is_access_in_list(alias, fw_current_rule) == false) {
        amxc_var_add(cstring_t, fw_current_rule, alias);
    }
    retval = 0;
exit:
    amxc_var_clean(&ret);
    amxc_string_delete(&fw_id);
    return retval;
}

int httpaccess_firewall_add_access(const char* alias) {
    int retval = -1;
    amxd_object_t* access = NULL;
    uint8_t allowed_ipversion = 0;

    when_false_status(amxm_has_function("fw", "fw", "set_service"), exit, retval = 0);
    when_false_status(amxm_has_function("fw", "fw", "delete_service"), exit, retval = 0);
    when_str_empty(alias, exit);
    access = httpaccess_access_get_by_alias(alias);
    when_null(access, exit);
    when_false_status(HTTPACCESS_GET_ACCESS_PARAM(bool, access, "Enable"), exit, retval = 0);

    allowed_ipversion = HTTPACCESS_GET_ACCESS_PARAM(uint8_t, access, "IPVersion");
    if(allowed_ipversion == 0) {
        when_failed(httpaccess_firewall_add_access_ip_version(alias, access, 4), exit);
        when_failed(httpaccess_firewall_add_access_ip_version(alias, access, 6), exit);
    } else {
        when_failed(httpaccess_firewall_add_access_ip_version(alias, access, allowed_ipversion), exit);
    }

    retval = 0;
exit:
    if(retval != 0) {
        httpaccess_firewall_del_access(alias);
    }
    return retval;
}
