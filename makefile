include makefile.inc

NOW = $(shell date +"%Y-%m-%d(%H:%M:%S %z)")

# Extra destination directories
PKGDIR = ./output/$(MACHINE)/pkg/

define create_changelog
	@$(ECHO) "Update changelog"
	mv CHANGELOG.md CHANGELOG.md.bak
	head -n 9 CHANGELOG.md.bak > CHANGELOG.md
	$(ECHO) "" >> CHANGELOG.md
	$(ECHO) "## Release $(VERSION) - $(NOW)" >> CHANGELOG.md
	$(ECHO) "" >> CHANGELOG.md
	$(GIT) log --pretty=format:"- %s" $$($(GIT) describe --tags | grep -v "merge" | cut -d'-' -f1)..HEAD  >> CHANGELOG.md
	$(ECHO) "" >> CHANGELOG.md
	tail -n +10 CHANGELOG.md.bak >> CHANGELOG.md
	rm CHANGELOG.md.bak
endef

# targets
all:
	$(MAKE) -C src all

clean:
	$(MAKE) -C src clean
	$(MAKE) -C test clean

install: all
	$(INSTALL) -D -p -m 0644 odl/tr181-httpaccess.odl $(DEST)/etc/amx/tr181-httpaccess/tr181-httpaccess.odl
ifeq ($(CONFIG_GATEWAY),y)
	$(INSTALL) -D -p -m 0644 odl/tr181-httpaccess_target-gw.odl $(DEST)/etc/amx/tr181-httpaccess/tr181-httpaccess_target-gw.odl
endif
ifeq ($(CONFIG_ACCESSPOINT),y)
	$(INSTALL) -D -p -m 0644 odl/tr181-httpaccess_target-ap.odl $(DEST)/etc/amx/tr181-httpaccess/tr181-httpaccess_target-ap.odl
endif
ifeq ($(CONFIG_GATEWAY),y)
	$(INSTALL) -d -m 0755 $(DEST)/etc/amx/tr181-httpaccess
	ln -sfr $(DEST)/etc/amx/tr181-httpaccess/tr181-httpaccess_target-gw.odl $(DEST)/etc/amx/tr181-httpaccess/tr181-httpaccess_target.odl
endif
ifeq ($(and $(CONFIG_ACCESSPOINT),$(if $(CONFIG_GATEWAY),,y)),y)
	$(INSTALL) -d -m 0755 $(DEST)/etc/amx/tr181-httpaccess
	ln -sfr $(DEST)/etc/amx/tr181-httpaccess/tr181-httpaccess_target-ap.odl $(DEST)/etc/amx/tr181-httpaccess/tr181-httpaccess_target.odl
endif
ifeq ($(CONFIG_SAH_AMX_TR181_HTTPACCESS_FIREWALL),y)
	$(INSTALL) -D -p -m 0644 odl/extensions/00_tr181-httpaccess_firewall.odl $(DEST)/etc/amx/tr181-httpaccess/extensions/00_tr181-httpaccess_firewall.odl
endif
	$(INSTALL) -D -p -m 0644 odl/tr181-httpaccess_definition.odl $(DEST)/etc/amx/tr181-httpaccess/tr181-httpaccess_definition.odl
	$(INSTALL) -D -p -m 0644 odl/tr181-userinterface_mapping.odl $(DEST)/etc/amx/tr181-device/extensions/01_device-userinterface_mapping.odl
	$(INSTALL) -D -p -m 0644 odl/tr181-httpaccess_session_definition.odl $(DEST)/etc/amx/tr181-httpaccess/tr181-httpaccess_session_definition.odl
	$(INSTALL) -D -p -m 0644 odl/tr181-userinterface_definition.odl $(DEST)/etc/amx/tr181-httpaccess/tr181-userinterface_definition.odl
	$(INSTALL) -d -m 0755 $(DEST)//etc/amx/$(COMPONENT)/defaults.d
	$(foreach odl,$(wildcard odl/defaults.d/*.odl), $(INSTALL) -D -p -m 0644 $(odl) $(DEST)/etc/amx/$(COMPONENT)/defaults.d/;)
	$(INSTALL) -D -p -m 0644 output/$(MACHINE)/object/tr181-httpaccess.so $(DEST)/usr/lib/amx/tr181-httpaccess/tr181-httpaccess.so
	$(INSTALL) -d -m 0755 $(DEST)$(BINDIR)
	ln -sfr $(DEST)$(BINDIR)/amxrt $(DEST)$(BINDIR)/tr181-httpaccess
	$(INSTALL) -D -p -m 0755 scripts/tr181-httpaccess.sh $(DEST)$(INITDIR)/$(COMPONENT)

package: all
	$(INSTALL) -D -p -m 0644 odl/tr181-httpaccess.odl $(PKGDIR)/etc/amx/tr181-httpaccess/tr181-httpaccess.odl
ifeq ($(CONFIG_GATEWAY),y)
	$(INSTALL) -D -p -m 0644 odl/tr181-httpaccess_target-gw.odl $(PKGDIR)/etc/amx/tr181-httpaccess/tr181-httpaccess_target-gw.odl
endif
ifeq ($(CONFIG_ACCESSPOINT),y)
	$(INSTALL) -D -p -m 0644 odl/tr181-httpaccess_target-ap.odl $(PKGDIR)/etc/amx/tr181-httpaccess/tr181-httpaccess_target-ap.odl
endif
ifeq ($(CONFIG_GATEWAY),y)
	$(INSTALL) -d -m 0755 $(PKGDIR)/etc/amx/tr181-httpaccess
	rm -f $(PKGDIR)/etc/amx/tr181-httpaccess/tr181-httpaccess_target.odl
	ln -sfr $(PKGDIR)/etc/amx/tr181-httpaccess/tr181-httpaccess_target-gw.odl $(PKGDIR)/etc/amx/tr181-httpaccess/tr181-httpaccess_target.odl
endif
ifeq ($(and $(CONFIG_ACCESSPOINT),$(if $(CONFIG_GATEWAY),,y)),y)
	$(INSTALL) -d -m 0755 $(PKGDIR)/etc/amx/tr181-httpaccess
	rm -f $(PKGDIR)/etc/amx/tr181-httpaccess/tr181-httpaccess_target.odl
	ln -sfr $(PKGDIR)/etc/amx/tr181-httpaccess/tr181-httpaccess_target-ap.odl $(PKGDIR)/etc/amx/tr181-httpaccess/tr181-httpaccess_target.odl
endif
ifeq ($(CONFIG_SAH_AMX_TR181_HTTPACCESS_FIREWALL),y)
	$(INSTALL) -D -p -m 0644 odl/extensions/00_tr181-httpaccess_firewall.odl $(PKGDIR)/etc/amx/tr181-httpaccess/extensions/00_tr181-httpaccess_firewall.odl
endif
	$(INSTALL) -D -p -m 0644 odl/tr181-httpaccess_definition.odl $(PKGDIR)/etc/amx/tr181-httpaccess/tr181-httpaccess_definition.odl
	$(INSTALL) -D -p -m 0644 odl/tr181-userinterface_mapping.odl $(PKGDIR)/etc/amx/tr181-device/extensions/01_device-userinterface_mapping.odl
	$(INSTALL) -D -p -m 0644 odl/tr181-httpaccess_session_definition.odl $(PKGDIR)/etc/amx/tr181-httpaccess/tr181-httpaccess_session_definition.odl
	$(INSTALL) -D -p -m 0644 odl/tr181-userinterface_definition.odl $(PKGDIR)/etc/amx/tr181-httpaccess/tr181-userinterface_definition.odl
	$(INSTALL) -d -m 0755 $(PKGDIR)//etc/amx/$(COMPONENT)/defaults.d
	$(INSTALL) -D -p -m 0644 odl/defaults.d/*.odl $(PKGDIR)/etc/amx/$(COMPONENT)/defaults.d/
	$(INSTALL) -D -p -m 0644 output/$(MACHINE)/object/tr181-httpaccess.so $(PKGDIR)/usr/lib/amx/tr181-httpaccess/tr181-httpaccess.so
	$(INSTALL) -d -m 0755 $(PKGDIR)$(BINDIR)
	rm -f $(PKGDIR)$(BINDIR)/tr181-httpaccess
	ln -sfr $(PKGDIR)$(BINDIR)/amxrt $(PKGDIR)$(BINDIR)/tr181-httpaccess
	$(INSTALL) -D -p -m 0755 scripts/tr181-httpaccess.sh $(PKGDIR)$(INITDIR)/$(COMPONENT)
	cd $(PKGDIR) && $(TAR) -czvf ../$(COMPONENT)-$(VERSION).tar.gz .
	cp $(PKGDIR)../$(COMPONENT)-$(VERSION).tar.gz .
	make -C packages

changelog:
	$(call create_changelog)

test:
	$(MAKE) -C test run
	$(MAKE) -C test coverage

.PHONY: all clean changelog install package test