#!/bin/sh

source /usr/lib/amx/scripts/amx_init_functions.sh
[ -f /etc/environment ] && source /etc/environment
ulimit -c ${ULIMIT_CONFIGURATION:-0}

name="tr181-httpaccess"
datamodel_root="UserInterface"

case $1 in
    boot)
        process_boot ${name} -D
        ;;
    start)
        process_start ${name} -D
        ;;
    stop)
        process_stop ${name}
        ;;
    shutdown)
        process_shutdown ${name}
        ;;
    restart)
        $0 stop
        $0 start
        ;;
    debuginfo)
        process_debug_info ${datamodel_root}
        controller="$(ba-cli -al "protected;${datamodel_root}.Controller?" | grep 'mod-')"
        case "$controller" in
            mod-httpaccess-lighttpd)
                lighttpd_config_file="$(grep 'conf-file = ' /etc/amx/${name}/${name}_target.odl | cut -d \" -f 2)"
                lighttpd -p -f "$lighttpd_config_file"
                ;;
            *)
                ;;
        esac
        ;;
    *)
        echo "Usage : $0 [start|boot|stop|shutdown|debuginfo|restart]"
        ;;
esac
