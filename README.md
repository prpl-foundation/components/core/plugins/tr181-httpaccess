# tr181-httpaccess

[TOC]

# Introduction

tr181-httpaccess is a plugin to manage the user interface (web-ui)

# Overview

tr181-httpaccess is built on top of the <code>Ambiorix</code> framework. Please consult
the <code>Ambiorix</code> documentation for more information about the used data
types.

tr181-httpaccess depends on libnetmodel to query the network stack.

tr181-httpaccess depends on several libamxm modules:

## HTTPAccessController
By default mod-httpaccess-lighttpd will be used to create <code>Lighttpd</code>'s
configurations and manage its life cycle.

Configurable in the data-model: UserInterface.Controller

## FWController
By default mod-fw-amx will be used to open/close firewall ports.

Configurable in the odl (tr181-httpaccess_target.odl): firewall.controller

# Building, installing and testing

## Docker container

You could install all tools needed for testing and developing on your local
machine, or use a pre-configured environment. Such an environment is already
prepared for you as a docker container.

1. Install Docker

    Docker must be installed on your system. Here are some links that could
    help you:

    - [Get Docker Engine - Community for
      Ubuntu](https://docs.docker.com/install/linux/docker-ce/ubuntu/)
    - [Get Docker Engine - Community for
      Debian](https://docs.docker.com/install/linux/docker-ce/debian/)
    - [Get Docker Engine - Community for
      Fedora](https://docs.docker.com/install/linux/docker-ce/fedora/)
    - [Get Docker Engine - Community for
      CentOS](https://docs.docker.com/install/linux/docker-ce/centos/)
      <br /><br />

    Make sure you user id is added to the docker group:

        sudo usermod -aG docker $USER
    <br />

2. Fetch the container image

    To get access to the pre-configured environment, pull the image and launch
    a container.

    Pull the image:

        docker pull registry.gitlab.com/soft.at.home/docker/oss-dbg:latest

    Before launching the container, you should create a directory which will be
    shared between your local machine and the container.

        mkdir -p ~/amx/libraries/

    Launch the container:

        docker run -ti -d \
            --name oss-dbg \
            --restart=always \
            --cap-add=SYS_PTRACE \
            --sysctl net.ipv6.conf.all.disable_ipv6=1 \
            -e "USER=$USER" \
            -e "UID=$(id -u)" \
            -e "GID=$(id -g)" \
            -v ~/amx:/home/$USER/amx \
            registry.gitlab.com/soft.at.home/docker/oss-dbg:latest

    The `-v` option bind mounts the local directory for the amx project in
    the container, at the exact same place.
    The `-e` options create environment variables in the container. These
    variables are used to create a user name with exactly the same user id and
    group id in the container as on your local host (user mapping).

    You can open as many terminals/consoles as you like:

        docker exec -ti --user $USER oss-dbg /bin/bash

## Building

### Prerequisites

<code>tr181-httpaccess</code> depends on the following libraries:

- libamxb
- libamxc
- libamxd
- libamxm
- libamxo
- libamxp
- libsahtrace
- libnetmodel
- mod-fw-amx
- mod-httpaccess-lighttpd

These libraries can be installed in the container:

    sudo apt-get install libamxb libamxc libamxd libamxm libamxo libamxp mod-sahtrace sah-lib-sahtrace-dev libnetmodel mod-fw-amx

Currently there isn't any package for mod-httpaccess-lighttpd, refer to the module's repository to install it in a container.

### Build tr181-httpaccess

1. Clone the git repository

    To be able to build it, you need the source code. So open the directory
    just created for the ambiorix project and clone this library in it (on your
    local machine).

        cd ~/amx/plugins/
        git clone git@gitlab.com:prpl-foundation/components/core/plugins/tr181-httpaccess.git

2. Build it

    cd ~/amx/plugins/tr181-httpaccess
    make

## Installing

### Using make target install

You can install your own compiled version easily in the container by running
the install target.

    cd ~/amx/plugins/tr181-httpaccess
    sudo make install

### Using package

From within the container you can create packages.

    cd ~/amx/plugins/tr181-httpaccess
    make package

The packages generated are:

    ~/amx/plugins/tr181-httpaccess/tr181-httpaccess_<VERSION>.tar.gz
    ~/amx/plugins/tr181-httpaccess/tr181-httpaccess_<VERSION>.deb

You can copy these packages and extract/install them.

For Ubuntu or Debian distributions use dpkg:

    sudo dpkg -i ~/amx/plugins/tr181-httpaccess/tr181-httpaccess_<VERSION>.deb

## Testing

### Prerequisites

No extra components are needed for testing `tr181-httpaccess`.

### Run tests

You can run the tests by executing the following command:

    cd ~/amx/plugins/tr181-httpaccess/test
    make

Or this command if you also want the coverage tests to run:

    cd ~/amx/plugins/tr181-httpaccess/test
    make run coverage

You can combine both commands:

    cd ~/amx/plugins/tr181-httpaccess
    make test

### Coverage reports

The coverage target will generate coverage reports using
[gcov](https://gcc.gnu.org/onlinedocs/gcc/Gcov.html) and
[gcovr](https://gcovr.com/en/stable/guide.html).

A summary for each c-file is printed in your console after the tests are run.
A HTML version of the coverage reports is also generated. These reports are
available in the output directory of the compiler used.  For example, when using
native gcc, the output of `gcc -dumpmachine` is `x86_64-linux-gnu`, the HTML
coverage reports can be found at
`~/amx/plugins/tr181-httpaccess/output/x86_64-linux-gnu/coverage/report.`

You can easily access the reports in your browser. In the container start a
python3 http server in background.

    cd ~/amx/
    python3 -m http.server 8080 &

Use the following url to access the reports:

    http://<IP ADDRESS OF YOUR CONTAINER>:8080/plugins/tr181-httpaccess/output/<MACHINE>/coverage/report

You can find the ip address of your container by using the `ip -br a` command
in the container.

Example:

    USER@<CID>:~/amx/plugins/tr181-httpaccess$ ip -br a
    lo              UNKNOWN        127.0.0.1/8
    eth0            UP             172.17.0.3/16


In this case the ip address of the container is `172.17.0.3`.  So the url you
should use is:
`http://172.17.0.3:8080/plugins/tr181-httpaccess/output/x86_64-linux-gnu/coverage/report/`

## Documentation

### Prerequisites

To generate the documentation, [Doxygen](https://www.doxygen.nl) is required
and already available in the container. In case you want to install this on
your local machine:

    sudo apt-get install doxygen

### Paths

The documentation is split in two parts, starting from the repository path:

    cd ~/amx/plugins/tr181-httpaccess

Here, you can find:

- README.md: this file is used on Gitlab and is the main page for the Doxygen documentation.
- docs directory: contains the Doxygen settings, code examples and additional pages used in Doxygen.

The code itself is documented in the approriate header and source files.

### Generate documentation

You can generate the documentation by executing the following command:

    cd ~/amx/plugins/tr181-httpaccess
    make doc

The full documentation is available in the `output` directory. You can easily
access the documentation in your browser. As described in the coverage reports
section, you can start a http server in the container.

    cd ~/amx/
    python3 -m http.server 8080 &

Use the following url to access the reports:

    http://<IP ADDRESS OF YOUR CONTAINER>:8080/plugins/tr181-httpaccess/output/html/index.html

You can find the ip address of your container by using the `ip -br a` command
in the container.

Example:

    USER@<CID>:~/amx/plugins/tr181-httpaccess$ ip -br a
    lo              UNKNOWN        127.0.0.1/8
    eth0            UP             172.17.0.3/16


In this case the ip address of the container is `172.17.0.3`.  So the url you
should use is:
`http://172.17.0.3:8080/plugins/tr181-httpaccess/output/html/index.html`

## Run

After building and installing, all needed files are installed in your file system.

A symbolic link, called `tr181-httpaccess`, is installed in `/usr/bin/`. To launch the `tr181-httpaccess` application use following command:

```bash
$ tr181-httpaccess
```

The application will then configure all the accesses using the controller as described in the data-model.

### Prerequisites

The `tr181-httpaccess` plugin during execution uses other plugins features:
- Netmodel stack: HTTPAccess objects are tied to an IP.Interface.{i}. and to start an HTTP access this interface needs to be up,
it uses libnetmodel to be notified when a change occurs for the tied interface.
- tr181-firewall: `tr181-httpaccess` uses mod-amx-fw to open/close port for the accesses
- tr181-usermanagement: UserInterface object exposes an RPC called CheckCredentialsForAccess it uses tr181-usermanagement to
validate credentials and roles authorization
- tr181-lanconfigsecurity: UserInterface object exposes an RPC called PasswordReset() which resets `LANConfigSecurity.ConfigPassword`
to it's factory value

To have the plugin running correctly make sure those plugins are started and accessible in your environment.

Since `tr181-httpaccess` configure accesses directly after being started. It needs `tr181-firewall` to be started before launched. To ensure this, this line has been added:
```odl
requires "Firewall.";
```

### Configuration options

The tr181-httpaccess application can be configured without recompilation, using an odl file config section.

The configuration options that can be set are:

- `apply-delay`: Some HTTP servers needs to be restarted to be reconfigured. To avoid numerous restart after multiple accesses are changed simultaneously. You can configure this variable with a value (in milliseconds) to only asks the controller to apply the new configuration after this delay has expired and reduce the number of times the server restarts. Can be set to 0 to apply directly.
- `firewall`: a table containing the following settings:
    - `controller`: Contains the absolute path to the firewall controller amxm module.
- `ip_intf_lo`: The tr181 path to the IP interface for the loopback interface
- `ip_intf_wan`: The tr181 path to the IP interface for the wan interface
- `ip_intf_lan`: The tr181 path to the IP interface for the lan interface
- `ip_intf_guest`: The tr181 path to the IP interface for the guest interface
- `ip_intf_lcm`: The tr181 path to the IP interface for the lcm interface

NOTE : `lo`, `wan`, `lan`, `guest` and `lcm` are deprecated in favor of above references

The tr181-httpaccess application can also configure its controller without recompilation, using an odl file config section.
The all configuration is passed to the controller module which will access the configuration variable it needs. Refer to the module repository to see what options are configurable.

To facilitate per target's configuration, those configuration options are located in the file `odl/tr181-httpaccess_target.odl`. This way you only need to override this file to configure this plugin.

Example:

```odl
%config {
//tr181-httpaccess plugin config
    %global apply-delay = 100; //In milliseconds
    %global firewall = {
        controller = "/usr/lib/amx/modules/mod-fw-amx.so"
    };
//mod configurations
    %global mod-lighttpd = {
        conf-file = "/etc/lighttpd/mod-lighttpd.conf",
        mod-dir = "/etc/lighttpd/mod/",
        conf-template = "/etc/lighttpd/lighttpd.template"
    };

// interface references for use in default ODLs
    %global lo = "Device.IP.Interface.1.";
    %global wan = "Device.IP.Interface.2.";
    %global lan = "Device.IP.Interface.3.";
    %global guest = "Device.IP.Interface.4.";
    %global lcm = "Device.IP.Interface.5.";
}
```

## RPC

### UserInterface.PasswordReset()

This RPC can be used to reset `LANConfigSecurity.ConfigPassword` to it's factory value.

### UserInterface.CheckCredentialsForAccess

This RPC can be used to validate if a user has access to an access based on the `HTTPAccess.{i}.AllowedRoles` and the `Users.User.{i}.RoleParticipation` and if its credentials are valid.

### UserInterface.HTTPAccess.{i}.CreateWebSession

This RPC can be used to create a websession for the HTTPAccess.
It will create an `UserInterface.HTTPAccess.{i}.Session.{i}.` containing informations about the user and a session ID that will be used later by the user for authentication.

### UserInterface.HTTPAccess.{i}.CheckSessionValid

This RPC can be used to validate if a session is valid for the `UserInterface.HTTPAccess.{i}.` and tied to the good IP.

## Limitations

### HTTPS

Options about HTTPS exist but are not implemented.
